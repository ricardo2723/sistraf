INSTALANDO O SISTRAFIN

REQUISITOS DO SISTEMA PARA INSTALAÇÃO
1 - PHP >= 7.1.3
2 - COMPOSER
3 - MYSQL
4 - OPENSSL PHP EXTENSION
5 - PDO PHP EXTENSION
6 - MBSTRING PHP EXTENSION
7 - TOKENIZER PHP EXTENSION
8 - XML PHP EXTENSION
9 - GIT BASH

INICIANDO A INSTALAÇÃO
1 - ACESSE A PASTA DO SEU SERVIDOR WEB, NO MEU CASO E /var/www/html
2 - CLONE O PROJETO DO SISTEMA:
	git clone https://ricardo2723@bitbucket.org/ricardo2723/sistraf.git
3 - ACESSE A PASTA sistraf/
4 - INSTALE TODAS AS DEPENDENCIAS DO PROJETO EXECUTANDO O COMANDO ABAIXO:
	composer install
5 - ACESSE O ARQUIVO NA PASTA sitraf/config/database.php E MODIFIQUE CONFORME AS CONFIGURAÇÕES DO SEU BANCO:

	'mysql' => [
                'driver' => 'mysql',
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => env('DB_DATABASE', 'MEUBANCO'),
                'username' => env('DB_USERNAME', 'MEUUSUARIODOBANCO'),
                'password' => env('DB_PASSWORD', 'MINHASENHA'),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ],
6 - INSTALE O BANCO DE DADOS USANDO O COMANDO ABAIXO
    php artisan migrate --seed
7 - LIBERE O ACESSO A PASTA DO SISTRAF:
	chmod -r 777 sistraf/

SELVA! SISTEMA INSTALADO COM SUCESSO
	
