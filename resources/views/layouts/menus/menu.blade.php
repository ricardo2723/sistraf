@can('admin')
    <li class="treeview">
        <a href="#">
            <i class="fa fa-users"></i> <span>Usuarios</span>
            <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('usuarios.index') }}"><i class="fa fa-user"></i> <span> Listar </span></a></li>
            <li><a href="{{ route('acessos.index') }}"><i class="fa fa-unlock"></i> <span> Perfil de Acesso </span></a>
            </li>
        </ul>
    </li>
    <li><a href="{{ route('postograduacao.index') }}"><i class="fa fa-angle-double-up"></i>
            <span> Posto / Graduação </span></a></li>
    <li><a href="{{ route('motoristas.index') }}"><i class="fa fa-truck"></i> <span> Motorista </span></a></li>
    <li><a href="{{ route('chefeviatura.index') }}"><i class="fa fa-user-o"></i> <span> Chefe de Viatura </span></a>
    </li>
    <li><a href="{{ route('viaturas.index') }}"><i class="fa fa-car"></i> <span> Viaturas </span></a></li>

    <li class="treeview">
        <a href="#">
            <i class="fa fa-angle-double-up"></i> <span>Comandate da Guarda</span>
            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
        </a>
        <ul class="treeview-menu">
            <li class="active"><a href="{{ route('dashboard.cmtgda') }}"><i class="fa fa-dashboard"></i> <span>DashBoard </span></a>
            </li>
            <li><a href="{{ route('registroViaturas.index') }}"><i class="fa fa-car"></i> <span> Fichas Viaturas </span></a>
            </li>
            <li><a href="{{ route('registroMilitares.index') }}"><i class="fa fa-angle-double-up"></i> <span> Fichas Militares </span></a>
            </li>
            <li><a href="#"><i class="fa fa-male"></i> <span> Fichas Visitantes </span></a></li>
        </ul>
    </li>

    {{--  OFICIAL DE DIA  --}}
    <li class="treeview">
        <a href="#">
            <i class="fa fa-star-o"></i> <span>Oficial de Dia</span>
            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
        </a>
        <ul class="treeview-menu">
            <li class="active"><a href="{{ route('dashboard.ofidia') }}"><i class="fa fa-dashboard"></i> <span>DashBoard </span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-list"></i> Fichas
                    <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-car"></i> <span>Fichas Viaturas </span></a>
                    </li>
                    <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-street-view"></i> <span>Fichas Visitantes </span></a>
                    </li>
                    <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-child"></i> <span>Fichas Visitantes Praça</span></a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-database"></i> Relatórios
                    <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-car"></i> <span>Relatórios Viaturas </span></a>
                    </li>
                    <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-street-view"></i> <span>Relatórios Visitantes </span></a>
                    </li>
                    <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-child"></i> <span>Relatórios Visitantes Praça</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li style="background-color: #9fa000">
        <a href="#"
           onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i> <span> Sair </span></a>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
@endcan()

@if(isset(Auth::user()->roles[0]->special) && Auth::user()->roles[0]->special != 'all-access')
    @can('cmtgda')
        @include('layouts.menus.menuCmtGda')
    @endcan()

    @can('ofdia')
        @include('layouts.menus.menuOfDia')
    @endcan()

    @can('gestor')
        @include('layouts.menus.menuGestor')
    @endcan()
@endif