<li class="active"><a href="{{ route('dashboard.ofidia') }}"><i class="fa fa-dashboard"></i> <span>DashBoard </span></a></li>
<li><a href="{{ route('registroViaturaOficial.trocarCmtGda') }}"><i class="fa fa-user"></i> <span>Trocar Cmt Gda </span></a></li>
<li><a href="{{ route('viaturas.index') }}"><i class="fa fa-cab"></i> <span>Viaturas </span></a></li>
<li class="treeview">
        <a href="#">
          <i class="fa fa-list"></i> <span>Fichas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-car"></i> <span>Fichas Viaturas </span></a></li>
            <li><a href="{{ route('registroMilitarOficial.index') }}"><i class="fa fa-angle-double-up"></i> <span> Fichas Militares </span></a></li>
            <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-child"></i> <span>Fichas Visitantes Praça</span></a></li>
        </ul>
</li>
<li class="treeview">
            <a href="#">
              <i class="fa fa-database"></i> <span>Relatórios</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('registroViaturaOficial.viaturasPdf') }}" ><i class="fa fa-car"></i> <span>Relatórios Viaturas </span></a></li>
                <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-street-view"></i> <span>Relatórios Visitantes </span></a></li>
                <li><a href="{{ route('registroViaturaOficial.index') }}"><i class="fa fa-child"></i> <span>Relatórios Visitantes Praça</span></a></li>
            </ul>
</li>
<li style="background-color: #9fa000"><a href="{{ route('servico.showCloseService') }}"><i class="fa fa-external-link"></i> <span> Sair </span></a></li>
