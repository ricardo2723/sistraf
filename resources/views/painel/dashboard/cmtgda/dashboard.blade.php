@extends('layouts.mastertop')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- Viaturas -->
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-chevron-circle-down"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Serviço</span>
                            <span class="info-box-text">Do Dia: {{ isset($servico['servico']->formatted_data_inicio) ? $servico['servico']->formatted_data_inicio : 'Serviço não foi Aberto' }}</span>
                            <span class="info-box-text">Para o Dia: {{ isset($servico['servico']->formatted_data_final) ? $servico['servico']->formatted_data_final : 'Serviço não foi Aberto' }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-star-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Oficial de dia</span>
                            <span class="info-box-text">{{ isset($servico['servico']->user->username) ? $servico['servico']->user->username : 'Serviço não foi Aberto' }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-user-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Cmt Guarda</span>
                            <span class="info-box-text">{{ isset($servico['servico']->userCmt->username) ? $servico['servico']->userCmt->username : 'Serviço não foi Aberto' }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <!-- Viaturas -->
                            <div class="row">
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-black-gradient">
                                        <div class="inner">
                                            <h3>{{ isset($viaturas['viaturasServicoOld']) ? $viaturas['viaturasServicoOld']->count() : 0 }}</h3>
                                            <p>Serviço Anterior</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>{{ isset($viaturas['viaturasServico']) ? $viaturas['viaturasServico']->count() : 0 }}</h3>
                                            <p>Viaturas no Serviço</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-3 col-xs-5">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>{{ isset($viaturas['viaturasForaServico']) ? $viaturas['viaturasForaServico']->count() : 0 }}
                                                <sup style="font-size: 20px"></sup></h3>
                                            <p>Viaturas fora da OM</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-truck"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3>{{ isset($viaturas['viaturasRetornoServico']) ? $viaturas['viaturasRetornoServico']->count() : 0 }}</h3>
                                            <p>Viaturas Retornaram</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-cab"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                            </div>

                            <!-- MILITARES -->
                            <div class="row">
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>{{ isset($militars['militarsServico']) ? $militars['militarsServico']->count() : 0 }}</h3>
                                            <p>Militares no Serviço</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-angle-double-up"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>{{ isset($militars['militarsEntrada']) ? $militars['militarsEntrada']->count() : 0 }}<sup
                                                        style="font-size: 20px"></sup></h3>
                                            <p>Militares no Quartel
                                            </p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-angle-up"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3>{{ isset($militars['militarsSaida']) ? $militars['militarsSaida']->count() : 0 }}</h3>
                                            <p>Militares Sairam</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                            </div>

                            <!-- Visitantes Pracinha-->
                            <div class="row">
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>0</h3>
                                            <p>Visitantes Praça no Serviço</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-child"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>0<sup style="font-size: 20px"></sup></h3>
                                            <p>Visitantes Dentro
                                            </p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-street-view"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3>0</h3>
                                            <p>Visitantes Sairam</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        {{-- <a href="#" class="small-box-footer">
                                          Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                        </a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endsection

        @push('datatables-script')
            <!-- DataTables -->
                <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>
    @endpush