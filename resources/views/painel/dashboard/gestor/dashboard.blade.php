@extends('layouts.mastertop')

@section('content')

    <div style="height: 80px" class="callout callout-success">

        <div class="col-md-2">
            <h4>Serviço do Dia</h4>
            <p>
                <i class="fa fa-calendar"> </i> {{ isset($servico->formatted_data_inicio) ? $servico->formatted_data_inicio : ' Serviço não Foi Aberto!' }}
            </p>
        </div>

        <div class="col-md-2">
            <h4>Para o Dia</h4>
            <p>
                <i class="fa fa-calendar"> </i> {{ isset($servico->formatted_data_final) ? $servico->formatted_data_final : ' Serviço não Foi Aberto!' }}
            </p>
        </div>

        <div class="col-md-2">
            <h4>Oficial de Dia</h4>
            <p>
                <i class="fa fa-star-o"> </i> {{ isset($servico->user->username) ? $servico->user->username : ' Serviço não Foi Aberto!' }}
            </p>
        </div>

        <div class="col-md-3">
            <h4>Comandante da Guarda</h4>
            <p>
                <i class="fa fa-user-o"> </i> {{ isset($servico->userCmt->username) ? $servico->userCmt->username : ' Serviço não Foi Aberto!' }}
            </p>
        </div>
    </div>

    <!-- BAR CHART -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Movimento Semanal</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <div class="box-body chart-responsive">
            <div id="chart_div" style="height:350px"></div>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- Viaturas -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registro de Viaturas</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">

                    <!-- Viaturas -->
                    <div class="row">
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-black-gradient">
                                <div class="inner">
                                    <h3>{{ isset($viaturasServicoOld) ? $viaturasServicoOld->count() : 0 }}</h3>
                                    <p>Do Serviço Anterior</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-car"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{ isset($viaturasServico) ? $viaturasServico->count() : 0 }}</h3>
                                    <p>No Serviço Atual</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-car"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-5">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{ isset($viaturasForaServico) ? $viaturasForaServico->count() : 0 }}
                                        <sup style="font-size: 20px"></sup></h3>
                                    <p>Fora da OM</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-truck"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{ isset($viaturasRetornoServico) ? $viaturasRetornoServico->count() : 0 }}</h3>
                                    <p>Retornaram</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cab"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        @if(isset($servico->user->username))
                            <div class="col-lg-2 col-xs-6">
                                <!-- small box -->
                                <!-- small box -->
                                <div class="small-box bg-green">
                                    <a href="{{ route('gestor.dashboardViaturasPdf') }}"
                                       class="small-box bg-green">
                                        <div class="inner">
                                            <h3><i class="fa fa-database"></i></h3>
                                            <p>Gerar PDF</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- ########### VISITANTES OM ########### --}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registro de Visitantes na OM</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <!-- Viaturas -->
                    <div class="row">
                        <!-- Visitantes -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>0</h3>
                                    <p>No Serviço Atual</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-street-view"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>0<sup style="font-size: 20px"></sup></h3>
                                    <p>No interior da OM
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>0</h3>
                                    <p>Sairam</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-male"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <a href="{{ route('registroViaturaOficial.viaturasPdf') }}"
                                   class="small-box bg-red">
                                    <div class="inner">
                                        <h3><i class="fa fa-database"></i></h3>
                                        <p>Gerar PDF</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- ########### VISITANTES PRAÇA OM ########### --}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registro de Visitantes na OM</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <!-- Viaturas -->
                    <div class="row">
                        <!-- Visitantes -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>0</h3>
                                    <p>No Serviço Atual</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-child"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>0<sup style="font-size: 20px"></sup></h3>
                                    <p>No interior da OM
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-street-view"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>0</h3>
                                    <p>Sairam</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                {{-- <a href="#" class="small-box-footer">
                                  Mais Informações <i class="fa fa-arrow-circle-right"></i>
                                </a> --}}
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <a href="{{ route('registroViaturaOficial.viaturasPdf') }}"
                                   class="small-box bg-red">
                                    <div class="inner">
                                        <h3><i class="fa fa-database"></i></h3>
                                        <p>Gerar PDF</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('datatables-css')

@endpush

@push('datatables-script')
    <!-- google charts -->
    <script src="{{ asset('adminlte/charts/google/chartBar.js')}}"></script>

    <script>
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {


            var data = google.visualization.arrayToDataTable([
                ['', 'Viaturas', 'Civis'],
                @foreach($viaturasGraf as $vtrGraf)
                [' {{ date('d-m', strtotime($vtrGraf->odom_saida_data)) . ' | ' . $dia[date('w', strtotime($vtrGraf->odom_saida_data))] }}', {{ $vtrGraf->qtd }}, 4 ],

                @endforeach
            ]);

            var options = {
                chart: {
                    subtitle: 'Movimento nos ultimos 7 dias',
                }
            };

            var chart = new google.charts.Bar(document.getElementById('chart_div'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
@endpush