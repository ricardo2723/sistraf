@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box ">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    <h3><i class="fa fa-car"></i> Cadastrar Saida do Militar - {{ $registroMilitarSaida->getPostoName->name_abreviado . ' ' . $registroMilitarSaida->nome_guerra }}
                    </h3>
                    {!! Form::open(['route' => 'registroMilitarSaidas.store', 'method' => 'post']) !!}
                    <h5>Registrar todos os Militares que sairem da OM.</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            @include('layouts.alerts.validationAlert')
                            <div class="box-body">
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Data Saida</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::date('saida_data', $value = null, ['class' => 'form-control']) !!}
                                                {!! Form::hidden('registro_militar_id', $registroMilitarSaida->id) !!}
                                                {!! Form::hidden('user_id', Auth::user()->id) !!}
                                                {!! Form::hidden('data_inicio', $registroMilitarSaida->entrada_data, ['class' => 'form-control']) !!}
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Hora Saida</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                                {!! Form::text('saida_hora', $value = null, ['class' => 'form-control', 'id' => 'hora']) !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Cadastrar', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroMilitares.index') }}"
                       class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#hora').inputmask("h:s", {"mask": "99:99"}); //specifying options            
        });
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush