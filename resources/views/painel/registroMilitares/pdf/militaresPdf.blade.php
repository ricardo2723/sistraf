@extends('layouts.mastertop')

@section('reports')

    <section class="content-header">

    </section>

    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-car"></i> Relatório de Registro de Militares
                </h2>
            </div>
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <strong>Oficial de Dia</strong>
                <address>
                    <strong>{{ $militars->user->username }}</strong><br>
                    {{ $militars->user->name }}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Comandante da Guarda</strong>
                <address>
                    <strong>{{ $militars->userCmt->username }}</strong><br>
                    {{ $militars->userCmt->name }}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Data do Serviço</b><br>
                <b>Do Dia:</b> {{ $militars->formatted_data_inicio }}<br>
                <b>Para o Dia:</b> {{ $militars->formatted_data_final }}
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Militar</th>
                        <th>Data | Hora Entrada</th>
                        <th>Data | Hora Saida</th>
                        <th>Veiculo</th>
                        <th>Estacionamento</th>
                        <th>Selo / Lacre / Placa</th>
                        <th>Observação</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($servicoMilitars))
                        @foreach ($servicoMilitars as $registro)
                            <tr>
                                <td>{{ $registro->getPostoName->name_abreviado . ' ' . $registro->nome_guerra }}</td>
                                <td>{{ $registro->formatted_entrada_data . ' | ' . $registro->formatted_entrada_hora }}</td>
                                <td>{{ $registro->getSaidaMilitar->formatted_saida_data or null }}</td>
                                <td>{{ ($registro->veiculo == 1 ) ? 'CARRO' : 'MOTO'  }}</td>
                                <td>
                                    @switch($registro->estacionamento)
                                        @case(1)
                                        <p>OFICIAIS</p>
                                        @break
                                        @case(2)
                                        <p>SUB TENETE / SARGENTO</p>
                                        @break
                                        @case(3)
                                        <p>CABO / SOLDADO</p>
                                        @break
                                        @case(4)
                                        <p>VISITANTES</p>
                                        @break
                                    @endswitch
                                </td>
                                <td>{{ $registro->selo_lacre_placa }}</td>
                                <td>{{ isset($registro->observacao) ? $registroMilitar->observacao : 'Sem Observações' }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-xs-6">
                <p class="lead">Total de Militares na OM: {{ $servicoMilitars->count() }}</p>
            </div>
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a class="btn btn-primary btn-flat btn-danger pull-right"
                   href="{{ isset($id) ? route( $route , $id) : route($route) }}" target=_blank><i
                            class="fa fa-file-pdf"></i> Gerar Ficha Militar PDF</a>
            </div>
        </div>
    </section>
        @endsection
