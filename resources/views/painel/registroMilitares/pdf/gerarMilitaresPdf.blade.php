<title>Fichas de Viaturas</title>
<style type="text/css">
    .tg {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .tg td {
        font-family: Arial, sans-serif;
        font-size: 14px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: black;
    }

    .tg th {
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: black;
    }

    .tg .tg-g145 {
        font-family: "Times New Roman", Times, serif !important;;
        text-align: center;
        vertical-align: top;
        vertical-align: middle
    }

    .tg .tg-gh1y {
        font-weight: bold;
        font-family: "Times New Roman", Times, serif !important;
        text-align: center;
        vertical-align: middle;
        padding: padding-left: 2px;
        padding-right: 2px;
    }

    .tg .tg-gh1h {
        font-weight: bold;
        font-family: "Times New Roman", Times, serif !important;
        text-align: center;
        vertical-align: middle;
    }

    .tg .tg-z7id {
        font-size: 12px;
        font-family: "Times New Roman", Times, serif !important;
        text-align: center;
        vertical-align: top;
        padding: 10px;
    }

    .tg .tg-hu3l {
        font-weight: bold;
        font-size: 100%;
        font-family: "Times New Roman", Times, serif !important;;
        text-align: center;
        vertical-align: top;
    }

    .tg .tg-x4jz {
        font-weight: bold;
        font-family: "Times New Roman", Times, serif !important;;
        text-align: center;
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
<style>
    @page {
        margin: 190px 15px 50px 15px;
    }

    #header {
        position: fixed;
        left: 0px;
        top: -155px;
        right: 0px;
        height: 130px;
        text-align: center;
    }

    #header_content {
        position: fixed;
        left: 0px;
        top: -11px;
        right: 0px;
        height: 130px;
        text-align: center;
    }
</style>

<div id="header">
    <table class="tg" style="width: 100%">
        <tr>
            <th class="tg-gh1h" width="75">VISTO:<br><br>{{ $militars->user->username }}<br>____________<br>Of Dia</th>
            <th class="tg-z7id" colspan="5"><span style="font-weight:bold">MINISTÉRIO DA DEFESA </span><br><span
                        style="font-weight:bold">EXÉRCITO BRASILEIRO</span><br><span style="font-weight:bold">7º BATALHÃO DE ENGENHARIA DE COMBATE </span><br><span
                        style="font-weight:bold">(Batalhão de Engenheiros / 1855)</span><br><span
                        style="font-weight:bold;text-decoration:underline">BATALHÃO VISCONDE DE TAUNAY</span></th>
            <th class="tg-hu3l" colspan="5"><br>FICHA DE MOVIMENTO DE MILITARES<br><br>Serviço do
                Dia {{ $militars->formatted_data_inicio_dia }} para o dia {{ $militars->formatted_data_final_dia }}
                de {{ $militars->formatted_data_final_mes }} de 2018<br></th>
            <th class="tg-gh1h" width="70">VISTO:<br><br>{{ $militars->userCmt->username }}<br>____________<br>Cmt Gda
            </th>
        </tr>
        <tr> //320
            <td class="tg-x4jz" colspan="4" rowspan="2">MILITAR <br> (CONDUTOR)</td>
            <td class="tg-x4jz" colspan="2">DATA | HORÁRIO</td>
            <td class="tg-x4jz" colspan="2" rowspan="2">VEICULO</td>
            <td class="tg-x4jz" rowspan="2">ESTACIONAMENTO</td>
            <td class="tg-x4jz" rowspan="2">SELO / LACRE / PLACA</td>
            <td class="tg-x4jz" rowspan="2" colspan="2">OBSERVAÇÕES</td>
        </tr>
        <tr>
            <td class="tg-gh1y">ENTRADA</td>
            <td class="tg-gh1y" width="45">SAIDA</td>
        </tr>
        {{-- </table> --}}

        {{-- <table class="tg" style="width: 100%"> --}}
        @if(isset($servicoMilitars))
            @foreach ($servicoMilitars as $registro)
                <tr>
                    <td class="tg-g145" width="180"
                        colspan="4">{{ $registro->getPostoName->name_abreviado . ' ' . $registro->nome_guerra }}</td>
                    <td class="tg-g145"
                        width="90">{{ $registro->formatted_entrada_data . ' | ' . $registro->formatted_entrada_hora }}</td>
                    <td class="tg-g145"
                        width="90">{{ $registro->getSaidaMilitar->formatted_saida_data or 'NÃO SAIU' }}</td>
                    <td class="tg-g145" width="60" colspan="2">
                        @switch($registro->veiculo)
                            @case(1)
                            CARRO
                            @break
                            @case(2)
                            MOTO
                            @break
                        @endswitch
                    </td>
                    <td class="tg-g145" width="160">
                        @switch($registro->estacionamento)
                            @case(1)
                            OFICIAIS
                            @break
                            @case(2)
                            SUB TENETE / SARGENTO
                            @break
                            @case(3)
                            CABO / SOLDADO
                            @break
                            @case(4)
                            VISITANTES
                            @break
                        @endswitch
                    </td>
                    <td class="tg-g145" width="100">{{ $registro->selo_lacre_placa }}</td>
                    <td class="tg-g145" width="130" colspan="2">{{ isset($registro->observacao) ? $registroMilitar->observacao : 'SEM OBSERVAÇÃO' }}</td>
                </tr>
            @endforeach
        @endif
    </table>
</div>


<script>
    $('#navbar-topCasaFina').on('show.collapse', function () {
        $('.topCasaFina-banner').css('transform', 'translate(-50%, 10%)');
    })
</script>