@extends('layouts.mastertop')

@section('content')


    <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    <h3><i class="fa fa-car"></i> Corrigir Registro de Militar</h3>
                    {!! Form::model($corrigirRegistroMilitar, ['route' => ['registroMilitares.corrigirRegistroUpdate', $corrigirRegistroMilitar->id], 'method' => 'put']) !!}
                    <h5>Corrigir registro de Militar - Solicitado pelo Oficial de Dia</h5>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-body">
                            <div class="row">
                                @include('layouts.alerts.validationAlert')
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Menssage de Correção:</label>
                                        <p>
                                            <i class="fa fa-check text-green"></i> {{ $corrigirRegistroMilitar->msg_correcao }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label>Posto / Graduação</label>
                                        {!! Form::select('posto_id', isset($postos) ? $postos->pluck('name_abreviado', 'id') : ['1' => 'Registro'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                                    </div>
                                </div>
                                <div class="col-md-8 mb-3">
                                    <div class="form-group">
                                        <label>Nome de Guerra</label>
                                        {!! Form::text('nome_guerra', $value = null, ['class' => 'form-control', 'placeholder' => 'Nome de Guerra do Condutor']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Data Entrada:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('entrada_data', isset($corrigirRegistroMilitar->entrada_data) ? $corrigirRegistroMilitar->formatted_up_entrada_data : '' , ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Hora Entrada</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            {!! Form::time('entrada_hora', isset($corrigirRegistroMilitar->entrada_data) ? $corrigirRegistroMilitar->formatted_up_entrada_hora : '' , ['class' => 'form-control', 'id' => 'hora']) !!}
                                            {!! Form::hidden('service_id', isset($serv->id) ? $serv->id : null) !!}
                                            {!! Form::hidden('user_id', isset(\Auth::user()->id) ? \Auth::user()->id : null) !!}
                                            {!! Form::hidden('data_inicio', isset($serv->data_inicio) ? $serv->data_inicio : null) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Data Saida:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('saida_data', isset($corrigirRegistroMilitar->getSaidaMilitar->saida_data) ? $corrigirRegistroMilitar->getSaidaMilitar->formatted_up_saida_data : '' , ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Hora Saida</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            {!! Form::time('saida_hora', isset($corrigirRegistroMilitar->getSaidaMilitar->saida_data) ? $corrigirRegistroMilitar->getSaidaMilitar->formatted_up_saida_hora : '' , ['class' => 'form-control', 'id' => 'hora']) !!}
                                            {!! Form::hidden('id_saida', isset($corrigirRegistroMilitar->getSaidaMilitar->saida_data) ? $corrigirRegistroMilitar->getSaidaMilitar->id : '' ) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Veículo</label>
                                        <div class="radio">
                                            <label><input type="radio" name="veiculo"
                                                          value="1" {{ isset($corrigirRegistroMilitar) ? $corrigirRegistroMilitar->veiculo == 1 ? 'checked' : '' : ''  }}>
                                                CARRO
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="veiculo"
                                                          value="2" {{ isset($corrigirRegistroMilitar) ? $corrigirRegistroMilitar->veiculo == 2 ? 'checked' : '' : ''  }}>
                                                MOTO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estacionamento</label>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento"
                                                          value="1" {{ isset($corrigirRegistroMilitar) ? $corrigirRegistroMilitar->estacionamento == 1 ? 'checked' : '' : ''  }}>
                                                OFICIAL
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento"
                                                          value="2" {{ isset($corrigirRegistroMilitar) ? $corrigirRegistroMilitar->estacionamento == 2 ? 'checked' : '' : ''  }}>
                                                SUB TENENTE E SARGENTO
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento"
                                                          value="3" {{ isset($corrigirRegistroMilitar) ? $corrigirRegistroMilitar->estacionamento == 3 ? 'checked' : '' : ''  }}>
                                                CABO E SOLDADO
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento"
                                                          value="4" {{ isset($corrigirRegistroMilitar) ? $corrigirRegistroMilitar->estacionamento == 4 ? 'checked' : '' : ''  }}>
                                                VISISTANTES
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Selo / Lacre / Placa</label>
                                        {!! Form::text('selo_lacre_placa', $value = null, ['class' => 'form-control', 'placeholder' => 'Selo, lacre ou placa do Veiculo']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Observações</label>
                                        {!! Form::text('observacao', $value = null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Corrigir', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroMilitares.index') }}" class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-xs-3"></div>
    </div>

@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
@endpush