@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3><i class="fa fa-list"></i> Registro de Militares</h3>
                    Militares que entraram e sairam da OM
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead class="table_blue">
                        <tr>
                            <th>Militar</th>
                            <th>Data | Hora Entrada</th>
                            <th>Data | Hora Saida</th>
                            <th>Veiculo</th>
                            <th>Estacionamento</th>
                            <th>Selo / Lacre / Placa</th>
                            <th>Observação</th>
                            <th>Ação</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($registroMilitar_list))
                            @foreach($registroMilitar_list as $registro)
                                @if($registro->status === 2)
                                    <tr data-toggle="tooltip" data-placement="top" title="Aguardando Correção"
                                        style="background-color: #c6c700; font-weight: bold; vertical-align: middle">
                                @elseif($registro->status === 3)
                                    <tr data-toggle="tooltip" data-placement="top" title="Dados Corrigido pelo Cmt Gda"
                                        style="background-color: burlywood; vertical-align: middle">
                                @else
                                    <tr>
                                        @endif
                                        <td>{{ $registro->getPostoName->name_abreviado . ' ' . $registro->nome_guerra }}</td>
                                        <td>{{ $registro->formatted_entrada_data . ' | ' . $registro->formatted_entrada_hora }}</td>
                                        <td>{{ $registro->getSaidaMilitar->formatted_saida_data or null }}</td>
                                        <td>{{ ($registro->veiculo == 1 ) ? 'CARRO' : 'MOTO'  }}</td>
                                        <td>
                                            @switch($registro->estacionamento)
                                                @case(1)
                                                <p>OFICIAIS</p>
                                                @break
                                                @case(2)
                                                <p>SUB TENETE / SARGENTO</p>
                                                @break
                                                @case(3)
                                                <p>CABO / SOLDADO</p>
                                                @break
                                                @case(4)
                                                <p>VISITANTES</p>
                                                @break
                                            @endswitch
                                        </td>
                                        <td>{{ $registro->selo_lacre_placa }}</td>
                                        <td>{{ isset($registro->observacao) ? $registro->observacao : 'Sem Observações' }}</td>
                                        @if($registro->status === 2)
                                            <td style="text-align: center; width: 65pt">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <i class='fas fa-shield-alt text-blue '></i></a>
                                                </div>
                                            </td>
                                        @elseif($registro->status === 0)
                                            <td data-toggle="tooltip" data-placement="top"
                                                title="Militar Ainda não Saiu"
                                                style="text-align: center; font-size: 20px; width: 65pt">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <i class='fa fa-sign-out text-warning'></i></a>
                                                </div>
                                            </td>
                                        @else
                                            <td style="text-align: center; width: 65pt">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitarOficial.show', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Corrigir Registro"> <i
                                                                class='fa fa-edit text-blue'></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitarOficial.finalizarRegistro', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Finalizar Registro"><i class='fa fa-check text-green'></i></a>
                                                </div>
                                            </td>
                                        @endif
                                        <td>{{ $registro->status }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "order": [[8, "desc"]],
                "columnDefs": [{
                    "targets": [8],
                    "visible": false,
                }]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush