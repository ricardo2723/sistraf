@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    @if(isset($registroMilitarEdit))
                        <h3><i class="fa fa-car"></i> Alterar Entrada de Militares</h3>
                        {!! Form::model($registroMilitarEdit, ['route' => ['registroMilitares.update', $registroMilitarEdit->id], 'method' => 'put']) !!}
                    @else
                        <h3><i class="fa fa-car"></i> Cadastrar Entrada de Militares</h3>
                        {!! Form::open(['route' => 'registroMilitares.store', 'method' => 'post']) !!}
                    @endif
                    <h5>Registrar todas as Viaturas que sai da OM.</h5>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        @include('layouts.alerts.validationAlert')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label>Posto / Graduação</label>
                                        {!! Form::select('posto_id', isset($postos) ? $postos->pluck('name_abreviado', 'id') : ['1' => 'Registro'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                                    </div>
                                </div>
                                <div class="col-md-8 mb-3">
                                    <div class="form-group">
                                        <label>Nome de Guerra</label>
                                        {!! Form::text('nome_guerra', $value = null, ['class' => 'form-control', 'placeholder' => 'Nome de Guerra do Condutor']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Data Entrada:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('entrada_data', isset($registroMilitarEdit->entrada_data) ? $registroMilitarEdit->formatted_up_entrada_data : '' , ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Hora Entrada</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            {!! Form::text('entrada_hora', isset($registroMilitarEdit->entrada_data) ? $registroMilitarEdit->formatted_up_entrada_hora : '' , ['class' => 'form-control', 'id' => 'hora']) !!}
                                            {!! Form::hidden('service_id', isset($serv->id) ? $serv->id : null) !!}
                                            {!! Form::hidden('user_id', isset(\Auth::user()->id) ? \Auth::user()->id : null) !!}
                                            {!! Form::hidden('data_inicio', isset($serv->data_inicio) ? $serv->data_inicio : null) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Veículo</label>
                                        <div class="radio">
                                            <label><input type="radio" name="veiculo" value="1" {{ isset($registroMilitarEdit) ? $registroMilitarEdit->veiculo == 1 ? 'checked' : '' : ''  }}>
                                                CARRO
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="veiculo" value="2" {{ isset($registroMilitarEdit) ? $registroMilitarEdit->veiculo == 2 ? 'checked' : '' : ''  }}>
                                                MOTO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estacionamento</label>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento" value="1" {{ isset($registroMilitarEdit) ? $registroMilitarEdit->estacionamento == 1 ? 'checked' : '' : ''  }}>
                                                OFICIAL
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento" value="2" {{ isset($registroMilitarEdit) ? $registroMilitarEdit->estacionamento == 2 ? 'checked' : '' : ''  }}>
                                                SUB TENENTE E SARGENTO
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento" value="3" {{ isset($registroMilitarEdit) ? $registroMilitarEdit->estacionamento == 3 ? 'checked' : '' : ''  }}>
                                                CABO E SOLDADO
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="estacionamento" value="4" {{ isset($registroMilitarEdit) ? $registroMilitarEdit->estacionamento == 4 ? 'checked' : '' : ''  }}>
                                                VISISTANTES
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Selo / Lacre / Placa</label>
                                        {!! Form::text('selo_lacre_placa', $value = null, ['class' => 'form-control', 'placeholder' => 'Selo, lacre ou placa do Veiculo']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Observações</label>
                                        {!! Form::text('observacao', $value = null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(isset($registroViaturaEdit) ? 'Alterar' : 'Cadastrar', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroMilitares.index') }}"
                       class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-xs-3"></div>
    </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
@endpush

@push('datatables-script')
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>

        $(document).ready(function () {
            $('#hora').inputmask("h:s", {"mask": "99:99"}); //specifying options            
        });

    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
@endpush 