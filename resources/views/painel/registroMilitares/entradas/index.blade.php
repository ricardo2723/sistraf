@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box">
                <div class="box-header with-border">
                    <h3><i class="fa fa-list"></i> Registro de Movimento de Militares</h3>
                    Militares que entraram e sairam da OM
                    <a class="btn btn-primary btn-flat btn-oliva pull-right"
                       href="{{ route('registroMilitares.create') }}"><i class="fa fa-car"></i> Registrar Militares</a>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead class="table_blue">
                        <tr>
                            <th>Militar</th>
                            <th>Data | Hora Entrada</th>
                            <th>Data | Hora Saida</th>
                            <th>Veiculo</th>
                            <th>Estacionamento</th>
                            <th>Selo / Lacre / Placa</th>
                            <th>Observação</th>
                            <th>Ação</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($registroMilitares))
                            @foreach($registroMilitares as $registro)
                                @if($registro->status === 2)
                                    <tr data-toggle="tooltip" data-placement="top"
                                        title="{{ $registro->msg_correcao }}"
                                        style="background: #c6c700; font-weight: bold">
                                @else
                                    <tr>
                                        @endif
                                        <td>{{ $registro->getPostoName->name_abreviado . ' ' . $registro->nome_guerra }}</td>
                                        <td>{{ $registro->formatted_entrada_data . ' | ' . $registro->formatted_entrada_hora }}</td>
                                        <td>{{ $registro->getSaidaMilitar->formatted_saida_data or null }}</td>
                                        <td>{{ ($registro->veiculo == 1 ) ? 'CARRO' : 'MOTO'  }}</td>
                                        <td>
                                            @switch($registro->estacionamento)
                                                @case(1)
                                                <p>OFICIAIS</p>
                                                @break
                                                @case(2)
                                                <p>SUB TENETE / SARGENTO</p>
                                                @break
                                                @case(3)
                                                <p>CABO / SOLDADO</p>
                                                @break
                                                @case(4)
                                                <p>VISITANTES</p>
                                                @break
                                            @endswitch
                                        </td>
                                        <td>{{ $registro->selo_lacre_placa }}</td>
                                        <td>{{ isset($registro->observacao) ? $registroMilitar->observacao : 'Sem Observações' }}</td>

                                        <td style="text-align: center; width: 65pt">
                                            @if($registro->status === 0 )
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitares.edit', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Editar Registro"> <i
                                                                class='fa fa-pencil text-primary'></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitarSaidas.show', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Registrar Saida do Militar"><i
                                                                class='fa fa-angle-double-up text-yellow'></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitares.show', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Excluir Registro"> <i
                                                                class='fa fa-trash text-red'></i></a>
                                                </div>
                                            @elseif($registro->status === 2)
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitares.corrigirRegistro', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Corrigir Registro"> <i
                                                                class='fa fa-edit text-primary'></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroMilitares.show', $registro->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Excluir Registro"> <i
                                                                class='fa fa-trash text-danger'></i></a>
                                                </div>
                                            @else
                                                <div style="margin-right:5px" class="btn-group">
                                                    <i data-toggle="tooltip" data-placement="top"
                                                       title="Enviado para Oficial de Dia"
                                                       class='fas fa-shield-alt text-blue'></i>
                                                </div>
                                            @endif
                                        </td>
                                        <td>{{ $registro->status }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "language": {
                    "url": "/adminlte/components/datatables.net/js/ptBr.lang"
                },
                "order": [[8, "desc"]],
                "columnDefs": [{
                    "targets": [8],
                    "visible": false,
                }]
            });
        });
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush