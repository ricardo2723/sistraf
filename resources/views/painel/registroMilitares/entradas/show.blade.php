@extends('layouts.mastertop')

@section('content')


    <!-- Modal -->
    <div class="modal fade custom-modal" data-backdrop="static" id="modal-sucess" tabindex="1" role="dialog"
         aria-labelledby="customModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
                <div class="modal-header-delete">
                    <h3>Excluir Registro da Militar</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body">
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Militar:</label>
                                            <p>{{ $registroMilitar->getPostoName->name_abreviado . ' ' . $registroMilitar->nome_guerra }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Data | Hora Entrada:</label>
                                            <p>{{ $registroMilitar->formatted_entrada_data . ' | ' . $registroMilitar->formatted_entrada_hora }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Data | Hora Saida:</label>
                                            <p>{{ $registroMilitar->saida_data }}</p>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="col-md-3    ">
                                        <div class="form-group">
                                            <label>Veiculo:</label>
                                            @switch($registroMilitar->veiculo)
                                                @case(1)
                                                <p>CARRO</p>
                                                @break
                                                @case(2)
                                                <p>MOTO</p>
                                                @break
                                            @endswitch
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Estacionamento:</label>
                                            @switch($registroMilitar->estacionamento)
                                                @case(1)
                                                <p>OFICIAIS</p>
                                                @break
                                                @case(2)
                                                <p>SUB TENETE / SARGENTO</p>
                                                @break
                                                @case(3)
                                                <p>CABO / SOLDADO</p>
                                                @break
                                                @case(4)
                                                <p>VISITANTES</p>
                                                @break
                                            @endswitch
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Selo | Lacre | Placa:</label>
                                            <p>{{ $registroMilitar->selo_lacre_placa }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Observação:</label>
                                            <p>{{ isset($registroMilitar->observacao) ? $registroMilitar->observacao : 'Sem Observações' }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route' => ['registroMilitares.destroy', $registroMilitar->id], 'method' => 'DELETE']) !!}
                    {!! Form::submit('Excluir', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroMilitares.index') }}" class="btn btn-oliva flat">Voltar</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#modal-sucess').modal('show');
        });
    </script>
@endpush