@extends('layouts.mastertop')

@section('content')

    {{-- ########### SERVIÇO POR DATA ########### --}}

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Serviço Por Data</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">

                    @if(count($errors) != 0)
                        <div class="box-body">
                            <div class="alert alert-danger flat" >
                                @foreach ($errors->all() as $error)
                                    <p><i class="fa fa-times"></i> {{ $error }}</p>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    {!! Form::open(['route' => 'gestor.reportServicoShow', 'method' => 'post']) !!}
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data do Serviço</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::date('data_servico', $value = null, ['class' => 'form-control', 'required']) !!}
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><br></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::submit('Buscar', ['class' => 'btn flat btn-oliva']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    {!! Form::open(['route' => 'gestor.reportServicoDataShow', 'method' => 'post']) !!}
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data Inicial</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::date('data_inicio', $value = null, ['class' => 'form-control', 'required']) !!}
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data Final</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::date('data_final', $value = null, ['class' => 'form-control', 'required']) !!}
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><br></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::submit('Buscar', ['class' => 'btn flat btn-oliva']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>


    {{-- ############################## RELATÓRIO POR DATA  ############################## --}}


    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Numero de Registros Encontrados:   <span class="label label-info ">{{ isset($servico) ? $servico->count() : 0  }}</span> </h4>
                </div>

            </div>
        </div>
    </div>

    @if(isset($error))
        <div class="box">
            <div class="alert alert-danger flat">
                <p><i class="fa fa-times"></i> Sua busca não retornou dados.</p>
            </div>
        </div>
    @endif

    @if(isset($servico))
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detalhes do Serviço do Dia {{ $servico->formatted_data_inicio }}</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Oficial de Dia</label>
                                    <div class="input-group">
                                        <p>{{ $servico->user->username }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Comandante da Guarda</label>
                                    <div class="input-group">
                                        <p>{{ $servico->userCmt->username }}</p>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Viaturas no Serviço</label>
                                    <div class="input-group">
                                        <a href="{{ route('gestor.viaturasPdf', $servico->id) }}">
                                            <span class="label label-success">{{ $servico->registroViatura->count() }}</span>
                                        </a>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Visitantes no Serviço</label>
                                    <div class="input-group">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="label label-success">0</span>
                                        </a>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status do Serviço</label>
                                    <div class="input-group">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            @if($servico->status == 0)
                                                <span class="label label-danger ">Aberto</span>
                                            @else
                                                <span class="label label-success ">Fechado</span>
                                            @endif
                                        </a>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

    {{-- ############################## RELATÓRIO ENTRE DATAS  ############################## --}}

    @if(isset($servicoBetween))
        @foreach($servicoBetween as $servico)
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Detalhes do Serviço do
                                Dia {{ $servico->formatted_data_inicio }}</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Oficial de Dia</label>
                                        <div class="input-group">
                                            <p>{{ $servico->user->username }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Comandante da Guarda</label>
                                        <div class="input-group">
                                            <p>{{ $servico->userCmt->username }}</p>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Viaturas no Serviço</label>
                                        <div class="input-group">
                                            <a href="{{ route('gestor.viaturasPdf', $servico->id) }}">
                                                <span class="label label-success">{{ $servico->registroViatura->count() }}</span>
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Visitantes no Serviço</label>
                                        <div class="input-group">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span class="label label-success">0</span>
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Status do Serviço</label>
                                        <div class="input-group">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                @if($servico->status == 0)
                                                    <span class="label label-danger ">Aberto</span>
                                                @else
                                                    <span class="label label-success ">Fechado</span>
                                                @endif
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach()
    @endif

@endsection
