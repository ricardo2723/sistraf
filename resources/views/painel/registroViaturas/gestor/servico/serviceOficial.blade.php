@extends('layouts.mastertop')

@section('content')

    {{-- ########### SERVIÇO POR OFICIAL DE DIA ########### --}}
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Serviço Por Oficial de Dia</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'gestor.reportServicoOficialShow', 'method' => 'post']) !!}
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label>Oficial de Dia</label>
                                {!! Form::select('of_dia', isset($user_oficial) ? $user_oficial : ['0' => 'Não Possui Registros'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><br></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::submit('Buscar', ['class' => 'btn flat btn-oliva']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    {{-- ############################## RELATÓRIO POR OFICIAL DE DIA  ############################## --}}

    @if(session()->has('servico'))

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h4 class="box-title">Numero de Registros Encontrados:   <span class="label label-info ">{{ session('servico')[0]->count() }}</span> </h4>
                    </div>

                </div>
            </div>
        </div>

        @if(session()->has('error') && session('error')[0] != null)
            <div class="box">
                <div class="alert alert-danger flat">
                    <p><i class="fa fa-times"></i> Sua busca não retornou dados.</p>
                </div>
            </div>
        @endif

        @foreach(session('servico')[0] as $servico)
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Detalhes do Serviço do
                                Dia {{ $servico->formatted_data_inicio }}</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Oficial de Dia</label>
                                        <div class="input-group">
                                            <p>{{ $servico->user->username }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Comandante da Guarda</label>
                                        <div class="input-group">
                                            <p>{{ $servico->userCmt->username }}</p>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Viaturas no Serviço</label>
                                        <div class="input-group">
                                            <a href="{{ route('gestor.viaturasPdf', $servico->id) }}">
                                                <span class="label label-success">{{ $servico->registroViatura->count() }}</span>
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Visitantes no Serviço</label>
                                        <div class="input-group">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span class="label label-success">0</span>
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Status do Serviço</label>
                                        <div class="input-group">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                @if($servico->status == 0)
                                                    <span class="label label-danger ">Aberto</span>
                                                @else
                                                    <span class="label label-success ">Fechado</span>
                                                @endif
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach()
    @endif

@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>

@endpush

