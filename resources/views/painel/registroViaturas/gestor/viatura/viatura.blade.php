@extends('layouts.mastertop')

@section('content')

    {{-- ########### SERVIÇO POR DATA ########### --}}

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Serviço Por Viatura</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">

                    @if(count($errors) != 0)
                        <div class="box-body">
                            <div class="alert alert-danger flat">
                                @foreach ($errors->all() as $error)
                                    <p><i class="fa fa-times"></i> {{ $error }}</p>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    {!! Form::open(['route' => 'gestor.reportViaturaShow', 'method' => 'post']) !!}
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label>Viatura</label>
                                {!! Form::select('viatura', isset($viatura) ? $viatura : ['0' => 'Não Possui Registros'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::date('data_inicio', $value = null, ['class' => 'form-control']) !!}
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><br></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::submit('Buscar', ['class' => 'btn flat btn-oliva']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    {!! Form::open(['route' => 'gestor.reportViaturaDataShow', 'method' => 'post']) !!}
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-3 mb-3">
                            <div class="form-group">
                                <label>Viatura</label>
                                {!! Form::select('viatura', isset($viatura) ? $viatura : ['0' => 'Não Possui Registros'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data Inicial</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::date('data_inicio', $value = null, ['class' => 'form-control', 'required']) !!}
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data Final</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {!! Form::date('data_final', $value = null, ['class' => 'form-control', 'required']) !!}
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><br></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    {!! Form::submit('Buscar', ['class' => 'btn flat btn-oliva']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>


    {{-- ############################## RELATÓRIO POR DATA  ############################## --}}


    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Numero de Registros Encontrados: <span
                                class="label label-info ">{{ isset($viaturas) ? sizeof($viaturas) : 0  }}</span></h4>
                </div>

            </div>
        </div>
    </div>

    @if(isset($error))
        <div class="box">
            <div class="alert alert-danger flat">
                <p><i class="fa fa-times"></i> Sua busca não retornou dados.</p>
            </div>
        </div>
    @endif

    @if(isset($viaturas))
        @foreach($viaturas as $viatura)
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Detalhes da Viatura
                                - {{ strtoupper($viatura->eb_placa) . ' | ' . strtoupper($viatura->prefixo) }} </h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Motorista</label>
                                        <div class="input-group">
                                            <p>{{ $motoristasChefeViaturas->find($viatura->motorista_id)->nome_guerra }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Chefe Viatura</label>
                                        <div class="input-group">
                                            <p>{{ $motoristasChefeViaturas->find($viatura->chefe_viatura_id)->nome_guerra }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Destino</label>
                                        <div class="input-group">
                                            <p>{{ $viatura->destino }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Data Retorno</label>
                                        <div class="input-group">
                                            <p>{{ isset($viatura->odom_entrada_data) ? date('d/m/Y H:s', strtotime($viatura->odom_entrada_data)) : 'Não Retornou' }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Detalhes</label>
                                        <div class="input-group">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span class="label label-primary ">Visualizar</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach()
    @endif

    {{-- ############################## RELATÓRIO ENTRE DATAS  ############################## --}}

    @if(isset($viaturasBetween))
        @foreach($viaturasBetween as $viatura)
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Detalhes da Viatura
                                - {{ strtoupper($viatura->eb_placa) . ' | ' . strtoupper($viatura->prefixo) }} </h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Motorista</label>
                                        <div class="input-group">
                                            <p>{{ $motoristasChefeViaturas->find($viatura->motorista_id)->nome_guerra }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Chefe Viatura</label>
                                        <div class="input-group">
                                            <p>{{ $motoristasChefeViaturas->find($viatura->chfe_viatura_id)->nome_guerra }}</p>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Destino</label>
                                        <div class="input-group">
                                            <p>{{ $viatura->destino }}</p>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Data Retorno</label>
                                        <div class="input-group">
                                            <p>{{ date('d/m/Y H:s', strtotime($viatura->odom_entrada_data)) }}</p>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Detalhes</label>
                                        <div class="input-group">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span class="label label-primary ">Visualizar</span>
                                            </a>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach()
    @endif

@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>

@endpush