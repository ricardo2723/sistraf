<title>Fichas de Viaturas</title>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-g145{font-family:"Times New Roman", Times, serif !important;;text-align:center;vertical-align:top;vertical-align:middle}
    .tg .tg-gh1y{font-weight:bold;font-family:"Times New Roman", Times, serif !important;text-align:center;vertical-align:middle;padding: padding-left: 2px; padding-right: 2px;}
    .tg .tg-gh1h{font-weight:bold;font-family:"Times New Roman", Times, serif !important;text-align:center;vertical-align:middle;}
    .tg .tg-z7id{font-size:12px;font-family:"Times New Roman", Times, serif !important;text-align:center;vertical-align:top;padding: 10px;}
    .tg .tg-hu3l{font-weight:bold;font-size:100%;font-family:"Times New Roman", Times, serif !important;;text-align:center;vertical-align:top;}
    .tg .tg-x4jz{font-weight:bold;font-family:"Times New Roman", Times, serif !important;;text-align:center; padding-left: 5px; padding-right: 5px;}
</style>
<style>
        @page { margin: 190px 15px 50px 15px; }
        #header { position: fixed; left: 0px; top: -155px; right: 0px; height: 130px;  text-align: center; }
        #header_content { position: fixed; left: 0px; top: -11px; right: 0px; height: 130px;  text-align: center; }
      </style>

      <div id="header">
        <table class="tg" style="width: 100%">
          <tr>
            <th class="tg-gh1h" width="75">VISTO:<br><br>{{ $viaturas->user->username }}<br>____________<br>Of Dia</th>
            <th class="tg-z7id" width="100" colspan="3"><span style="font-weight:bold">MINISTÉRIO DA DEFESA </span><br><span style="font-weight:bold">EXÉRCITO BRASILEIRO</span><br><span style="font-weight:bold">7º BATALHÃO DE ENGENHARIA DE COMBATE </span><br><span style="font-weight:bold">(Batalhão de Engenheiros / 1855)</span><br><span style="font-weight:bold;text-decoration:underline">BATALHÃO VISCONDE DE TAUNAY</span></th>
            <th class="tg-hu3l" width="210" colspan="6"><br>CONTROLE DO MOVIMENTO DE VIATURAS <br>DO 7º BECMB <br><br>Serviço do  Dia {{ $viaturas->formatted_data_inicio_dia }} para o dia {{ $viaturas->formatted_data_final_dia }} de {{ $viaturas->formatted_data_final_mes }} de 2018<br></th>
            <th class="tg-gh1h" width="70">VISTO:<br><br>{{ $viaturas->userCmt->username }}<br>____________<br>Cmt Gda</th>
          </tr>
          <tr> //320
            <td class="tg-x4jz" colspan="3">DADOS DA VIATURA</td>
            <td class="tg-x4jz" width="96" rowspan="2">MOTORISTA</td>
            <td class="tg-x4jz" colspan="2">SAÍDA</td>
            <td class="tg-x4jz" colspan="2">ENTRADA</td>
            <td class="tg-x4jz" width="149" rowspan="2" >DESTINO</td>
            <td class="tg-x4jz" width="60" rowspan="2" colspan="2">CHEFE VIATURA</td>
          </tr>
          <tr>
            <td class="tg-gh1y" >Prefixo</td>
            <td class="tg-gh1y" width="45" >EB/Placa</td>
            <td class="tg-gh1y" >Cia</td>
            <td class="tg-gh1y" width="39">Km</td>
            <td class="tg-gh1y" width="74">Data</td>
            <td class="tg-gh1y" width="39">Km</td>
            <td class="tg-gh1y" width="73">Data</td>
          </tr>
            {{-- </table> --}}
      
  {{-- <table class="tg" style="width: 100%"> --}}
  @if(isset($servicoViaturas))
    @foreach ($servicoViaturas as $viaturas)
        <tr>
          <td class="tg-g145" width="80">{{ $viaturas->detalhesViatura->prefixo }}</td>
          <td class="tg-g145" width="49">{{ $viaturas->detalhesViatura->eb_placa }}</td>
          <td class="tg-g145" width="39">{{ $viaturas->cia }}</td>
          <td class="tg-g145" width="108">{{ $viaturas->motorista->getPostoName->name_abreviado . ' ' . $viaturas->motorista->nome_guerra }}</td>
          <td class="tg-g145" width="43">{{ $viaturas->odom_saida }}</td>
          <td class="tg-g145" width="80">{{ $viaturas->formatted_odom_saida_data . ' ' .$viaturas->formatted_up_odom_saida_hora }}</td>          
          @if(!isset($viaturas->entradaViatura->odom_entrada_data))
            <td class="tg-g145" width="124" colspan="2" > Não Retornou </td>
          @else
            <td class="tg-g145" width="43">{{ $viaturas->entradaViatura->odom_entrada }}</td>
            <td class="tg-g145" width="80">{{ $viaturas->entradaViatura->formatted_odom_entrada_data . ' ' . $viaturas->entradaViatura->formatted_up_odom_entrada_hora}}</td>
          @endif          
          <td class="tg-g145" width="163">{{ $viaturas->destino }}</td>
          <td class="tg-g145" width="112" colspan="2">{{ $viaturas->chefeViatura->getPostoName->name_abreviado . ' ' . $viaturas->chefeViatura->nome_guerra }}</td>
        </tr>
    @endforeach
  @endif
</table>
</div>


<script>
    $('#navbar-topCasaFina').on('show.collapse', function() {
        $('.topCasaFina-banner').css('transform', 'translate(-50%, 10%)');
    })
</script>