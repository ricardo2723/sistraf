@extends('layouts.mastertop')

@section('reports')

    <section class="content-header">

    </section>

    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-car"></i> Relatório de Registro de Viaturas
                </h2>
            </div>
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <strong>Oficial de Dia</strong>
                <address>
                    <strong>{{ $viaturas->user->username }}</strong><br>
                    {{ $viaturas->user->name }}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Comandante da Guarda</strong>
                <address>
                    <strong>{{ $viaturas->userCmt->username }}</strong><br>
                    {{ $viaturas->userCmt->name }}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Data do Serviço</b><br>
                <b>Do Dia:</b> {{ $viaturas->formatted_data_inicio }}<br>
                <b>Para o Dia:</b> {{ $viaturas->formatted_data_final }}
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Dados da Viatura</th>
                        <th>Motorista</th>
                        <th>KM - Data Saida</th>
                        <th>KM - Data Entrada</th>
                        <th>KM Rodados</th>
                        <th>Destino</th>
                        <th>Chefe de Viatura</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($servicoViaturas))
                        @foreach ($servicoViaturas as $viatura)
                            <tr>
                                <td> {{ $viatura->detalhesViatura->prefixo }}
                                    - {{ $viatura->detalhesViatura->eb_placa }}</td>
                                <td> {{ $viatura->motorista->getPostoName->name_abreviado . ' ' . $viatura->motorista->nome_guerra }}</td>
                                <td> {{ $viatura->odom_saida }}
                                    - {{ $viatura->formatted_odom_saida_data . ' ' . $viatura->formatted_odom_saida_hora }} </td>
                                <td>
                                    @if(!isset($viatura->entradaViatura->odom_entrada_data))
                                        Não Retornou
                                    @else
                                        {{ $viatura->entradaViatura->odom_entrada }}
                                        - {{ $viatura->entradaViatura->formatted_odom_entrada_data . ' ' . $viatura->entradaViatura->formatted_up_odom_entrada_hora }}
                                    @endif

                                </td>
                                <td style="text-align: center">{{ isset($viatura->entradaViatura->odom_entrada) ? $viatura->entradaViatura->odom_entrada - $viatura->odom_saida : 0}}</td>
                                <td>{{ $viatura->destino }}</td>
                                <td>{{ $viatura->chefeViatura->getPostoName->name_abreviado . ' ' . $viatura->chefeViatura->nome_guerra }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-xs-6">
                <p class="lead">Total de Viaturas Movimentadas: {{ $servicoViaturas->count() }}</p>
            </div>
            <div class="col-xs-6">
                <p class="lead">Que não Retornaram: {{ $servicoViaturas->where('status', 0)->count()}}</p>
            </div>
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">

                <a class="btn btn-primary btn-flat btn-danger pull-right"
                   href="{{ isset($id) ? route( $route , $id) : route($route) }}" target=_blank><i
                            class="fa fa-file-pdf"></i> Gerar Ficha Viatura PDF</a>
            </div>
        </div>
    </section>
        @endsection
