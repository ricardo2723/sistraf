@extends('layouts.mastertop')

@section('content')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box ">
                            <div style="background-color: #808000; color:beige" class="box-header with-border">
                                    <h3><i class="fa fa-car"></i> Cadastrar Entrada da
                                            Viatura: {{ $registroViaturaEntrada->detalhesViatura->prefixo }}
                                            | {{ $registroViaturaEntrada->detalhesViatura->eb_placa }}</h3>
                                    {!! Form::open(['route' => 'registroViaturaEntradas.store', 'method' => 'post']) !!}
                                    <h5>Registrar todas as Viaturas que entram da OM.</h5>                                    
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                            @include('layouts.alerts.validationAlert')
                                        <div class="box-body">
                                            <div class="row">
                                                <!-- /.col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Odom Entrada</label>
                                                        {!! Form::hidden('registro_viatura_id', $registroViaturaEntrada->id) !!}
                                                        {!! Form::hidden('user_id', Auth::user()->id) !!}
                                                        {!! Form::hidden('odom_saida', $registroViaturaEntrada->odom_saida) !!}
                                                        {!! Form::text('odom_entrada', $value = null, ['class' => 'form-control', 'autofocus']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Data Entrada:</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            {!! Form::date('odom_entrada_data', $value = null, ['class' => 'form-control']) !!}
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Hora Entrada</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                            {!! Form::text('odom_entrada_hora', $value = null, ['class' => 'form-control', 'id' => 'hora']) !!}
                                                        </div>
                                                        {!! Form::hidden('date_in', $registroViaturaEntrada->odom_saida_data, ['class' => 'form-control']) !!}                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                {!! Form::submit('Cadastrar', ['class' => 'btn flat btn-oliva']) !!}
                                <a href="{{ route('registroViaturas.index') }}"
                                   class="btn btn-oliva flat">Fechar</a>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
        </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('#hora').inputmask("h:s", {"mask": "99:99"}); //specifying options            
        });  
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush