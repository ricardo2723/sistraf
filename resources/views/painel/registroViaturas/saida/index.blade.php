@extends('layouts.mastertop')

@section('content')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box">
                            <div class="box-header with-border">
                                <h3><i class="fa fa-list"></i> Registro das Viaturas</h3>
                                Viaturas que entraram e sairam da OM
                                <a class="btn btn-primary btn-flat btn-oliva pull-right"
                                   href="{{ route('registroViaturas.create') }}"><i class="fa fa-car"></i> Saida
                                    Viaturas</a>
                            </div>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead class="table_blue">
                                    <tr>
                                        <th></th>
                                        <th>Viatura</th>
                                        <th>Motorista</th>
                                        <th>Chefe Vtr</th>
                                        <th>Destino</th>
                                        <th>Km Saida</th>
                                        <th>Km Entrada</th>
                                        <th>Km Rodados</th>
                                        <th>Ação</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($registroViaturas))
                                        @foreach($registroViaturas as $registro)
                                            @if($registro->status === 2)
                                                <tr data-toggle="tooltip" data-placement="top"
                                                    title="{{ $registro->observacao }}" style="background: #c6c700; font-weight: bold">
                                            @else
                                                <tr>
                                                    @endif
                                                    <td style="text-align: center; font-size: 20px">
                                                        <a href="#" data-toggle="popover" data-trigger="focus"
                                                           data-placement="left" title="Dados da Viatura"
                                                           data-template='<div class="popover" role="tooltip"><div class="arrow"></div><h4 class="popover-header">Dados Complementares</h4><div class="popover-body"><table class="table table-bordered table-striped"><tbody><tr><th>Cia:</th><td>{{ $registro->cia }}</td></tr><tr><th>Viatura:</th><td>{{ $registro->detalhesViatura->modelo }}</td></tr><tr><th>Data Saida:</th><td>{{ $registro->formatted_odom_saida_data }}</td></tr><tr><th>Hora Sáida:</th><td>{{ $registro->formatted_odom_saida_hora }}</td></tr><tr><th>Data Retorno:</th><td>{{ $registro->entradaViatura->formatted_odom_entrada_data or null }}</td></tr><tr><th>Hora Retorno:</th><td>{{ $registro->entradaViatura->formatted_odom_entrada_hora or null }}</td></tr></tbody></table></div></div>'><i
                                                                    class="fa fa-plus-circle text-oliva"></i></a>
                                                    </td>
                                                    <td>{{ $registro->detalhesViatura->eb_placa }}</td>
                                                    <td>{{ $registro->motorista->getPostoName->name_abreviado . ' ' . $registro->motorista->nome_guerra }}</td>
                                                    <td>{{ $registro->chefeViatura->getPostoName->name_abreviado . ' ' .$registro->chefeViatura->nome_guerra }}</td>
                                                    <td>{{ $registro->destino }}</td>
                                                    <td>{{ $registro->odom_saida }}</td>
                                                    <td>{{ $registro->entradaViatura->odom_entrada or old('000000') }}</td>
                                                    <td style="text-align: center">
                                                        @if(isset($registro->entradaViatura->odom_entrada))
                                                            <span class="badge bg-green">{{ $registro->entradaViatura->odom_entrada - $registro->odom_saida }}</span></td>
                                                    @else
                                                        <span class="badge bg-red">0</span></td>
                                                    @endif

                                                    <td style="text-align: center; width: 65pt">
                                                        @if($registro->status === 0 )
                                                            <div style="margin-right:5px" class="btn-group">
                                                                <a href="{{ route('registroViaturas.edit', $registro->id) }}"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="Editar Registro"> <i
                                                                            class='fa fa-pencil text-primary'></i></a>
                                                            </div>
                                                            <div style="margin-right:5px" class="btn-group">
                                                                <a href="{{ route('registroViaturaEntradas.show', $registro->id) }}"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="Cadastrar Entrada de Viatura"><i
                                                                            class='fa fa-cab text-yellow'></i></a>
                                                            </div>
                                                            <div style="margin-right:5px" class="btn-group">
                                                                <a href="{{ route('registroViaturas.show', $registro->id) }}"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="Excluir Registro"> <i
                                                                            class='fa fa-trash text-red'></i></a>
                                                            </div>
                                                        @elseif($registro->status === 2)
                                                            <div style="margin-right:5px" class="btn-group">
                                                                <a href="{{ route('registroViatura.corrigirRegistro', $registro->id) }}"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="Corrigir Registro"> <i
                                                                            class='fa fa-edit text-primary'></i></a>
                                                            </div>
                                                            <div style="margin-right:5px" class="btn-group">
                                                                <a href="{{ route('registroViaturas.show', $registro->id) }}"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="Excluir Registro"> <i
                                                                            class='fa fa-trash text-danger'></i></a>
                                                            </div>
                                                        @else
                                                            <div style="margin-right:5px" class="btn-group">
                                                                <i data-toggle="tooltip" data-placement="top"
                                                                   title="Enviado para Oficial de Dia"
                                                                   class='fas fa-shield-alt text-blue'></i>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>{{ $registro->status }}</td>
                                                </tr>
                                                @endforeach
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "language": {
                    "url": "/adminlte/components/datatables.net/js/ptBr.lang"
                },
                "order": [[9, "desc"]],
                "columnDefs": [{
                    "targets": [9],
                    "visible": false,
                }]
            });
        });
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>    
@endpush