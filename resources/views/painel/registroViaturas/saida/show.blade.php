@extends('layouts.mastertop')

@section('content')


    <!-- Modal -->
    <div class="modal fade custom-modal" data-backdrop="static" id="modal-sucess" tabindex="1" role="dialog"
         aria-labelledby="customModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
                <div class="modal-header-delete">
                    <h3>Excluir Registro da Viatura: {{ $registroViatura->detalhesViatura->prefixo }}
                        | {{ $registroViatura->detalhesViatura->eb_placa }}</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body">
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Cia:</label>
                                            <p>{{ $registroViatura->cia }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Motorista:</label>
                                            <p>{{ $registroViatura->motorista->getPostoName->name_abreviado . ' ' . $registroViatura->motorista->nome_guerra }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Chefe Viatura:</label>
                                            <p>{{ $registroViatura->chefeViatura->getPostoName->name_abreviado . ' ' . $registroViatura->chefeViatura->nome_guerra }}</p>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Destino</label>
                                            <p>{{ $registroViatura->destino }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Odometro Saída</label>
                                            <p>{{ $registroViatura->odom_saida }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Odometro Entrada</label>
                                            <p>{{ $registroViatura->entradaViatura->odom_entrada or old('0000000') }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Data/Hora Saida</label>
                                            <p>{{ $registroViatura->formatted_odom_saida_data }}
                                                | {{ $registroViatura->formatted_odom_saida_hora }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Data/Hora Entrada</label>
                                            <p>{{ $registroViatura->entradaViatura->formatted_odom_entrada_data or old('00/00/2018') }}
                                                | {{ $registroViatura->entradaViatura->formatted_odom_entrada_hora or old('99:99') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::open(['route' => ['registroViaturas.destroy', $registroViatura->id], 'method' => 'DELETE']) !!}
                    {!! Form::submit('Excluir', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroViaturas.index') }}" class="btn btn-oliva flat">Voltar</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#modal-sucess').modal('show');
        });
    </script>
@endpush