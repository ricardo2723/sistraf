@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    @if(isset($registroViaturaEdit))
                        <h3><i class="fa fa-car"></i> Alterar Saida de Viatura</h3>
                        {!! Form::model($registroViaturaEdit, ['route' => ['registroViaturas.update', $registroViaturaEdit->id], 'method' => 'put']) !!}
                    @else
                        <h3><i class="fa fa-car"></i> Cadastrar Saida de Viatura</h3>
                        {!! Form::open(['route' => 'registroViaturas.store', 'method' => 'post']) !!}
                    @endif
                    <h5>Registrar todas as Viaturas que sai da OM.</h5>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        @include('layouts.alerts.validationAlert')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Registro EB / Placa</label>
                                        {!! Form::select('viatura_id', isset($viaturas) ? $viaturas : ['1' => 'Registro'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Cia</label>
                                        {!! Form::select(
                                                'cia',
                                                [
                                                    'CCAp'      => 'CCAp',
                                                    '1º Cia'    => '1º Cia',
                                                    'Cep'       => 'Cep',
                                                    'Cmdo'      => 'Cmdo',
                                                ],
                                                null,
                                                ['class' => 'form-control flat select2', 'style' => 'width: 100%']
                                            )
                                        !!}
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Motorista </label><a
                                                href="{{ route('registroViatura.motoristaCreate') }}"
                                                data-toggle="tooltip" data-placement="top"
                                                title="Cadastrar"> <i class="fa fa-plus-circle text-green"></i></a>
                                        <select name="motorista_id" class="form-control flat select2">
                                            @foreach($motorista as $moto)
                                                <option value='{{ $moto->id }}'>{{$moto->getPostoName->name_abreviado . ' ' . $moto->nome_guerra}}</option>
                                            @endforeach
                                        </select>
                                        <!-- {!! Form::select('motorista_id', isset($motorista) ? $motorista->pluck('nome_guerra', 'id') : 'Não Existe Registros', null, ['class' => 'form-control flat select2', 'style' => 'with: 100%']) !!}-->
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Chefe de Viatura </label>
                                        <a href="{{ route('registroViatura.chefeViaturaCreate') }}"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Cadastrar"> <i class="fa fa-plus-circle text-green"></i></a>
                                        <select name="chefe_viatura_id" class="form-control flat select2">
                                            @foreach($chefeViatura as $cv)
                                                <option value='{{ $cv->id }}'>{{$cv->getPostoName->name_abreviado . ' ' . $cv->nome_guerra}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label>Destino</label>
                                        {!! Form::text('destino', $value = null, ['class' => 'form-control', 'placeholder' => 'Destino da Viatura']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Odom Saida</label>
                                        {!! Form::text('odom_saida', $value = null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Data Saida:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('odom_saida_data', isset($registroViaturaEdit->odom_saida_data) ? $registroViaturaEdit->formatted_up_odom_saida_data : '' , ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Hora Saída</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            {!! Form::text('odom_saida_hora', isset($registroViaturaEdit->odom_saida_data) ? $registroViaturaEdit->formatted_up_odom_saida_hora : '' , ['class' => 'form-control', 'id' => 'hora']) !!}
                                            {!! Form::hidden('service_id', isset($serv->id) ? $serv->id : null) !!}
                                            {!! Form::hidden('user_id', isset(\Auth::user()->id) ? \Auth::user()->id : null) !!}
                                            {!! Form::hidden('data_inicio', isset($serv->data_inicio) ? $serv->data_inicio : null) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(isset($registroViaturaEdit) ? 'Alterar' : 'Cadastrar', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroViaturas.index') }}"
                       class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
@endpush

@push('datatables-script')
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>

        $(document).ready(function () {
            $('#hora').inputmask("h:s", {"mask": "99:99"}); //specifying options            
        });

    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
@endpush 