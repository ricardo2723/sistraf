@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3><i class="fa fa-list"></i> Registro das Viaturas</h3>
                    Viaturas que entraram e sairam da OM
                </div>
                <div class="box-body">
                    <table width="100%" id="example1" class="table table-bordered table-striped">
                        <thead class="table_blue">
                        <tr>
                            <th></th>
                            <th>Viatura</th>
                            <th>Motorista</th>
                            <th>Chefe Vtr</th>
                            <th>Destino</th>
                            <th>Odom Saida</th>
                            <th>Odom Entrada</th>
                            <th>Odom Total</th>
                            <th>Ação</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($registroViatura_list))
                            @foreach($registroViatura_list as $registroViatura)
                                @if($registroViatura->status === 2)
                                    <tr data-toggle="tooltip" data-placement="top" title="Aguardando Correção"
                                        style="background-color: #c6c700; font-weight: bold; vertical-align: middle">
                                @elseif($registroViatura->status === 3)
                                    <tr data-toggle="tooltip" data-placement="top" title="Dados Corrigido pelo Cmt Gda"
                                        style="background-color: burlywood; vertical-align: middle">
                                @else
                                    <tr>
                                        @endif
                                        <td style="text-align: center; font-size: 20px">
                                            <a href="#" data-toggle="popover" data-trigger="focus" data-placement="left"
                                               title="Dados da Viatura"
                                               data-template='<div class="popover" role="tooltip"><div class="arrow"></div><h4 class="popover-header">Dados Complementares</h4><div class="popover-body"><table class="table table-bordered table-striped"><tbody><tr><th>Cia:</th><td>{{ $registroViatura->cia }}</td></tr><tr><th>Viatura:</th><td>{{ $registroViatura->detalhesViatura->modelo }}</td></tr><tr><th>Data Saida:</th><td>{{ $registroViatura->formatted_odom_saida_data }}</td></tr><tr><th>Hora Sáida:</th><td>{{ $registroViatura->formatted_odom_saida_hora }}</td></tr><tr><th>Data Retorno:</th><td>{{ $registroViatura->entradaViatura->formatted_odom_entrada_data or null }}</td></tr><tr><th>Hora Retorno:</th><td>{{ $registroViatura->entradaViatura->formatted_odom_entrada_hora or null }}</td></tr></tbody></table></div></div>'><i
                                                        class="fa fa-plus-circle text-oliva"></i></a>
                                        </td>
                                        <td>{{ $registroViatura->detalhesViatura->eb_placa }}</td>
                                        <td>{{ $registroViatura->motorista->getPostoName->name_abreviado . ' ' . $registroViatura->motorista->nome_guerra }}</td>
                                        <td>{{ $registroViatura->chefeViatura->getPostoName->name_abreviado . ' ' . $registroViatura->chefeViatura->nome_guerra }}</td>
                                        <td>{{ $registroViatura->destino }}</td>
                                        <td>{{ $registroViatura->odom_saida }}</td>
                                        <td>{{ $registroViatura->entradaViatura->odom_entrada or old('000000') }}</td>
                                        <td style="text-align: center">
                                            @if(isset($registroViatura->entradaViatura->odom_entrada))
                                                <span class="badge bg-green">{{ $registroViatura->entradaViatura->odom_entrada - $registroViatura->odom_saida }}</span>
                                        </td>
                                        @else
                                            <span class="badge bg-red">0</span></td>
                                        @endif
                                        @if($registroViatura->status === 2)
                                            <td style="text-align: center; width: 65pt">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <i class='fas fa-shield-alt text-blue '></i></a>
                                                </div>
                                            </td>
                                        @elseif($registroViatura->status === 0)
                                            <td data-toggle="tooltip" data-placement="top"
                                                title="Viatura Ainda não Retornou"
                                                style="text-align: center; font-size: 20px; width: 65pt">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <i class='fa fa-sign-out text-warning'></i></a>
                                                </div>
                                            </td>
                                        @else
                                            <td style="text-align: center; width: 65pt">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroViaturaOficial.show', $registroViatura->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Corrigir Registro"> <i
                                                                class='fa fa-edit text-blue'></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('registroViaturaOficial.finalizarRegistro', $registroViatura->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Finalizar Registro"><i class='fa fa-check text-green'></i></a>
                                                </div>
                                            </td>
                                        @endif
                                        <td>{{ $registroViatura->status }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "order": [[9, "desc"]],
                "columnDefs": [{
                    "targets": [9],
                    "visible": false,
                }]
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush