@extends('layouts.mastertop')

@section('content')
<!-- CADASTRAR OBSERVAÇÃO -->
            <div class="row">
                <div class="col-xs-3">
                </div>
                <div class="col-xs-6">
                <div class="box box">
                    <div style="background-color: #808000; color:beige" class="box-header with-border">
                        <h3><i class="fas fa-user-cog"></i> Cadastrar a Observação do Registro de Viatura</h3>
                        <h5>Descreva os erros encontrados no registro</h5>                
                    {!! Form::open(['route' => ['registroViaturaOficial.edit', $id], 'method' => 'put']) !!} 
                </div>
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body">                           
                                <div class="row">                                                                       
                                    <div class="col-md-12 mb-3">
                                        <div class="form-group">
                                            {!! Form::textarea('observacao', $value = null, ['autofocus' => true, 'class' => 'form-control', 'required']) !!}
                                        </div>
                                    </div>                                        
                                </div>                                                                                                                     
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">                
                    {!! Form::submit('Salvar', ['class' => 'btn flat btn-oliva']) !!}                
                    <a href="{{ route('registroViaturaOficial.index') }}" class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}         
            </div>
        </div>
    </div>
</div>
</div>
</div>

@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
<!-- DataTables -->
<script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover()
    })
</script>

@endpush