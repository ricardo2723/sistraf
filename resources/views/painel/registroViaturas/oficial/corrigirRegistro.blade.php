@extends('layouts.mastertop')

@section('content')


    <div class="row">
        <div class="col-xs-12">
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    <h3><i class="fa fa-car"></i> Corrigir Registro de Viatura</h3>
                    {!! Form::model($corrigirRegistroViatura, ['route' => ['registroViatura.corrigirRegistroUpdate', $corrigirRegistroViatura->id], 'method' => 'put']) !!}
                    <h5>Corrigir registro de viatura - Solicitado pelo Oficial de Dia</h5>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-body">
                            <div class="row">
                                @include('layouts.alerts.validationAlert')
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Observação:</label>
                                        <p>
                                            <i class="fa fa-check text-green"></i> {{ $corrigirRegistroViatura->observacao }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Registro EB / Placa</label>
                                        {!! Form::select('viatura_id', isset($viatura_list) ? $viatura_list : ['1' => 'Registro'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Cia</label>
                                        {!! Form::select(
                                                'cia',
                                                [
                                                    'CCAp'      => 'CCAp',
                                                    '1º Cia'    => '1º Cia',
                                                    'Cep'       => 'Cep',
                                                    'Cmdo'      => 'Cmdo',
                                                ],
                                                null,
                                                ['class' => 'form-control flat select2', 'style' => 'width: 100%']
                                            )
                                        !!}
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Motorista </label><a
                                                href="{{ route('registroViatura.motoristaCreate' , 1) }}"
                                                data-toggle="tooltip" data-placement="top"
                                                title="Cadastrar"> <i class="fa fa-plus-circle text-green"></i></a>
                                        <select name="motorista_id" class="form-control flat select2">
                                            @foreach($motorista as $moto)
                                                <option value='{{ $moto->id }}'>{{$moto->getPostoName->name_abreviado . ' ' . $moto->nome_guerra}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <div class="form-group">
                                        <label>Chefe de Viatura </label>
                                        <a href="{{ route('registroViatura.motoristaCreate', 2) }}"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Cadastrar"> <i class="fa fa-plus-circle text-green"></i></a>
                                        <select name="chefe_viatura_id" class="form-control flat select2">
                                            @foreach($chefeViatura as $cv)
                                                <option value='{{ $cv->id }}'>{{$cv->getPostoName->name_abreviado . ' ' . $cv->nome_guerra}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label>Destino</label>
                                        {!! Form::text('destino', $value = null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Odom Saida</label>
                                        {!! Form::number('odom_saida', $value = null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Data Saida:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('odom_saida_data', $corrigirRegistroViatura->formatted_up_odom_saida_data, ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Hora Saída</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            {!! Form::time('odom_saida_hora', $value = $corrigirRegistroViatura->formatted_up_odom_saida_hora, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Odom Entrada</label>
                                        {!! Form::hidden('status', 3) !!}
                                        {!! Form::number('odom_entrada', $value = $corrigirRegistroViatura->entradaViatura->odom_entrada, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Data Entrada:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('odom_entrada_data', $value = $corrigirRegistroViatura->entradaViatura->formatted_up_odom_entrada_data, ['class' => 'form-control']) !!}
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Hora Entrada</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            {!! Form::time('odom_entrada_hora', $value = $corrigirRegistroViatura->entradaViatura->formatted_up_odom_entrada_hora, ['class' => 'form-control']) !!}
                                            {!! Form::hidden('id_entrada', $value = $corrigirRegistroViatura->entradaViatura->id, null) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Corrigir', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('registroViaturas.index') }}" class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

    @endsection

    @push('datatables-css')
        <!-- DataTables -->
            <link rel="stylesheet"
                  href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
            <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
            <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
            <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
    @endpush

    @push('datatables-script')
        <!-- DataTables -->
            <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
            <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
            <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
            <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
            <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
            <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
            <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
            <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

            <script>
                $(function () {
                    $("#example1").DataTable();
                    $('#example2').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": false,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false
                    });
                });

                //Datemask dd/mm/yyyy
                $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
                //Datemask2 mm/dd/yyyy
                $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
                //Money Euro
                $('[data-mask]').inputmask()

                //Timepicker
                $('.timepicker').timepicker({
                    showInputs: false
                })
            </script>

            <script>
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                    $('[data-toggle="popover"]').popover()
                })
            </script>
            <script>
                $(function () {
                    //Initialize Select2 Elements
                    $('.select2').select2()
                })
            </script>
    @endpush