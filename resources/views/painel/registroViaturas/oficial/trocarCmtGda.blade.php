@extends('layouts.mastertop')

@section('content')

<!-- Modal -->
<div class="modal fade custom-modal" data-backdrop="static" id="customModal" tabindex="1" role="dialog" aria-labelledby="customModal" aria-hidden="true">
    <div class="modal-dialog" style="width: 35em">
        <div class="modal-content">
            <div class="modal-header">
            <h3><i class="fa fa-user"></i> Subistituir Comandante da Guarda</h3>
            {!! Form::open(['route' => ['registroViaturaOficial.trocarCmtGdaUpdate', $serv->id], 'method' => 'put']) !!}
            <h5>Subistituir o Comandate da Guarda do Serviço</h5>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    @include('layouts.alerts.validationAlert')
                    <div class="col-xs-12">
                    <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-group">                                                
                                            <label>Comandante da Guarda</label>                                              
                                            {!! Form::select('cmt_user_id', isset($users) ? $users : ['0' => 'Vazio'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                                        </div>
                                    </div>                                    
                                </div>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <div class="form-group"> 
                                        {!! Form::submit('Alterar', ['class' => 'btn flat btn-oliva']) !!}
                                        <a href="{{ route('home') }}" class="btn btn-oliva flat">Voltar</a>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}         
        </div>
    </div>
</div>

@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })        
    </script>
    <script>
        $(document).ready(function () {
            $('#customModal').modal('show');
        });
    </script>
@endpush