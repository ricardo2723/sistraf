@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box">
                <div class="box-header with-border">
                    <h3><i class="fa fa-list"></i> Cadastro de Chefe de Viatura</h3>
                    Todos os chefe de viaturas da OM
                    <a class="btn btn-primary btn-flat btn-oliva pull-right"
                       href="{{ route('chefeviatura.create') }}"><i class="fa fa-users"></i> Cadastrar</a>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead class="table_blue">
                        <tr>
                            <th>Nome Completo</th>
                            <th>Nome de Guerra</th>
                            <th>Cpf</th>
                            <th>Status</th>
                            <th>Ação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($chefeViaturas))
                            @foreach($chefeViaturas as $chefeviatura)
                                <tr>
                                    <td>{{ $chefeviatura->nome_completo }}</td>
                                    <td>{{ $chefeviatura->getPostoName->name_abreviado . ' ' . $chefeviatura->nome_guerra  }}</td>
                                    <td>{{ isset($chefeviatura->cpf) ? $chefeviatura->cpf : 'Não Informado' }}</td>
                                    <td style="text-align: center">
                                        @if($chefeviatura->status == 1)
                                            <span class="badge bg-green">ATIVO</span></td>
                                    @else
                                        <span class="badge bg-red">INATIVO</span></td>
                                    @endif
                                    <td style="text-align: center">
                                        <div style="margin-right:5px" class="btn-group">
                                            <a href="{{ route('chefeviatura.edit', $chefeviatura->id) }}"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Editar Registro"> <i
                                                        class='fa fa-pencil text-primary'></i></a>
                                        </div>
                                        <div style="margin-right:5px" class="btn-group">
                                            <a href="{{ route('chefeviatura.show', $chefeviatura->id) }}"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Excluir Registro"> <i
                                                        class='fa fa-trash text-red'></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable(
                {
                    "language": {
                        "url": "/adminlte/components/datatables.net/js/ptBr.lang"
                    }
                }
            )
        });
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush