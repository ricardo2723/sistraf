@extends('layouts.mastertop')

@section('content')


    <!-- Modal -->
    <div class="modal fade custom-modal" data-backdrop="static" id="modal-sucess" tabindex="1" role="dialog"
         aria-labelledby="customModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px">
            <div class="modal-content">
                <div class="modal-header-delete">
                    <h3><i class="fa fa-warning text-yellow"></i> Atenção !!! </h3>
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body">
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h4><b>O Serviço ainda encontra-se aberto.</b></h4>
                                            <p><i class="fa fa-angle-right"></i> Não esqueça, se ja passou o serviço de fecha-lo no Sistema!</p>
                                            <p><i class="fa fa-angle-right"></i> Escolha uma das Opções Abaixo ?</p>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(session('success'))
                <div class="box-body">
                    <div class="alert alert-danger flat" >
                        <p><i class="fa fa-times"></i> {{ session('success')['messages'] }}</p>
                    </div>
                </div>
                @endif

                <div class="modal-footer">
                    {!! Form::open(['route' => 'logout', 'method' => 'POST']) !!}
                    {!! Form::submit('Sair', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('dashboard.ofidia') }}" class="btn btn-oliva flat">Voltar</a>
                    <a href="{{ route('servico.confirmaCloseService') }}" class="btn btn-oliva flat">Fechar Serviço</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <script>
        $(document).ready(function () {
            $('#modal-sucess').modal('show');
        });
    </script>
    
@endpush