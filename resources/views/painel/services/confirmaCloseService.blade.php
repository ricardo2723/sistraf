@extends('layouts.mastertop')

@section('content')


    <!-- Modal -->
    <div class="modal fade custom-modal" data-backdrop="static" id="modal-sucess" tabindex="1" role="dialog"
         aria-labelledby="customModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px">
            <div class="modal-content">
                <div class="modal-header-delete">
                    <h3><i class="fa fa-exclamation-triangle text-yellow"></i> Alerta !!! </h3>
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body">
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h4><b>Deseja Realmente Fechar o Serviço ?</b></h4>

                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(session('success'))
                <div class="box-body">
                    <div class="alert alert-danger flat" >
                        <p><i class="fa fa-times"></i> {{ session('success')['messages'] }}</p>
                    </div>
                </div>
                @endif

                <div class="modal-footer">
                    <a href="{{ route('servico.closeService') }}" class="btn btn-oliva flat">Sim</a>
                    <a href="{{ route('dashboard.ofidia') }}" class="btn btn-oliva flat">Não</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <script>
        $(document).ready(function () {
            $('#modal-sucess').modal('show');
        });
    </script>
    
@endpush