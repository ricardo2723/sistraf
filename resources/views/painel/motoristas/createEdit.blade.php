@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-xs-12 col-md-6">
            <div id="result"></div>
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    @if(isset($motorista))
                        <h3><i class="fa fa-car"></i> Alterar Motorista</h3>
                        {!! Form::model($motorista, ['route' => ['motoristas.update', $motorista->id], 'method' => 'put']) !!}
                    @else
                        <h3><i class="fa fa-car"></i> Cadastrar Motorista</h3>
                        {!! Form::open(['route' => 'motoristas.store', 'method' => 'post']) !!}
                    @endif
                    <h5>Registrar todos os Motoristas e Chefes de Viatura da OM.</h5>
                </div>

                @include('layouts.alerts.validationAlert')
                <div class="box-body">

                    <div class="form-group">
                        <label>Nome Completo</label>
                        {!! Form::text('nome_completo', $value = null, ['class' => 'form-control', 'autofocus']) !!}
                    </div>

                    <div class="form-group">
                        <label>Posto Graduação</label>
                        {!! Form::select('posto_id', $postoGraduacao->pluck('name', 'id'), null, ['class' => 'form-control flat select2 valor', 'custo' => 'custo1', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="form-group">
                        <label>Nome de Guerra</label>
                        {!! Form::text('nome_guerra', $value = null, ['class' => 'form-control', 'placeholder' => 'Somente o Nome de Guerra sem posto e graduação']) !!}
                    </div>

                    <div class="form-group">
                        <label>Cpf</label>
                        {!! Form::text('cpf', $value = null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label> Ativo </label>
                        <label>{!! Form::radio('status', 1, isset($motorista->status) ? ($motorista->status == 1) ? true : false : true) !!}</label>
                    </div>
                    <div class="form-group">
                        <label> Inativo </label>
                        <label>{!! Form::radio('status', 0, isset($motorista->status) ? ($motorista->status == 0) ? true : false : false) !!}</label>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(isset($motorista) ? 'Alterar' : 'Cadastrar', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('motoristas.index') }}"
                       class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@push('datatables-css')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
@endpush

@push('datatables-script')
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            })

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })


        })
    </script>

@endpush 