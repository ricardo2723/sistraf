@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-xs-12 col-md-4">
            <div id="result"></div>
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    @if(isset($postoGraduacao))
                        <h3><i class="fa fa-car"></i> Alterar Posto e Graduação</h3>
                        {!! Form::model($postoGraduacao,['route' => ['postograduacao.update', $postoGraduacao->id], 'method' => 'put']) !!}
                    @else
                        <h3><i class="fa fa-car"></i> Cadastrar Posto e Graduação</h3>
                        {!! Form::open(['route' => 'postograduacao.store', 'method' => 'post']) !!}
                    @endif
                    <h5>Cadastrar Posto e Graduação existente na OM.</h5>
                </div>
                <div class="box-body">
                    @include('layouts.alerts.validationAlert')
                    <div class="form-group">
                        <label for="prefixo">Posto: </label>
                        {!! Form::text('name', $value = null, ['class' => 'form-control', 'placeholder' => 'Ex: 3º SARGENTO', 'autofocus']) !!}
                    </div>
                    <div class="form-group">
                        <label for="prefixo">Abreviação: </label>
                        {!! Form::text('name_abreviado', $value = null, ['class' => 'form-control', 'placeholder' => 'Ex: 3SGT',]) !!}
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {!! Form::submit(isset($postoGraduacao) ? 'Alterar' : 'Cadastrar', ['class' => 'btn btn-oliva flat']) !!}
                    <a href="{{ route('postograduacao.index') }}" class="btn btn-oliva flat">Voltar</a>
                </div>
                {!! Form::close() !!}
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
@endsection