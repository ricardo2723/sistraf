@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3><i class="fa fa-car"></i> Cadastro de Posto e Graduação</h3>
                    Posto e Graduação disponiveis
                    <a class="btn btn-primary btn-flat btn-oliva pull-right"
                       href="{{ route('postograduacao.create') }}"><i class="fa fa-car"></i> Cadastrar Posto e Graduação</a>
                </div>

                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead class="table_blue">
                        <tr>
                            <th>Id</th>
                            <th>Posto</th>
                            <th>Abreviação</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($postoGraduacaos as $postograduacao)
                            <tr>
                                <td>{{ $postograduacao->id }}</td>
                                <td>{{ $postograduacao->name }}</td>
                                <td>{{ $postograduacao->name_abreviado }}</td>
                                <td style="text-align: center;">
                                    <div style="margin-right:5px" class="btn-group">
                                        <a href="{{ route('postograduacao.edit', $postograduacao->id) }}"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Editar {{ $postograduacao->name }}"><i
                                                    class="fa fa-pencil"></i></a>
                                    </div>
                                    <div style="margin-right:5px" class="btn-group">
                                        <a href="{{ route('postograduacao.show', $postograduacao->id) }}"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Excluir Registro"> <i
                                                    class='fa fa-trash text-red'></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable(
                {
                    "language": {
                        "url": "/adminlte/components/datatables.net/js/ptBr.lang"
                    }
                }
            )
        });
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
@endpush