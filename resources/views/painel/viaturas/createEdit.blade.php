@extends('layouts.mastertop')

@section('content')
<div class="row">
                <div class="col-md-3"></div>
                <div class="col-xs-12 col-md-6">
                        <div id="result"></div>
                        <div class="box box">
                                <div style="background-color: #808000; color:beige" class="box-header with-border">
                                        @if(isset($viatura))
                                            <h3><i class="fa fa-car"></i> Alterar Viatura</h3>
                                            {!! Form::model($viatura,['route' => ['viaturas.update', $viatura->id], 'method' => 'put']) !!}
                                        @else
                                            <h3><i class="fa fa-car"></i> Cadastrar Viatura</h3>
                                                {!! Form::open(['route' => 'viaturas.store', 'method' => 'post']) !!}
                                        @endif
                                            <h5>Cadastrar Viaturas da OM.</h5>                                            
                                    </div>                               
                                <div class="box-body">
                                        @include('layouts.alerts.validationAlert')
                                    <div class="form-group">
                                        <label for="prefixo">Modelo: </label>
                                        {!! Form::text('modelo', $value = null, ['class' => 'form-control', 'placeholder' => 'Modelo da Viatura', 'autofocus']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="prefixo">Prefixo: </label>
                                        {!! Form::text('prefixo', $value = null, ['class' => 'form-control', 'placeholder' => 'Tipo de Viatura', 'autofocus']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="ebPlaca">Registro Eb / Placa: </label>
                                        {!! Form::text('eb_placa', $value = null, ['class' => 'form-control', 'placeholder' => 'Registro Eb ou Placa']) !!}
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    {!! Form::submit(isset($viatura) ? 'Alterar' : 'Cadastrar', ['class' => 'btn btn-oliva flat']) !!}
                                    <a href="{{ route('viaturas.index') }}" class="btn btn-oliva flat">Voltar</a>
                                </div>
                                {!! Form::close() !!}
                        </div><!-- /.box -->
                    </div><!-- /.col -->           
                </div>
@endsection