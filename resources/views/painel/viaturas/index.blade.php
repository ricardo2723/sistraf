@extends('layouts.mastertop')

@section('content')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3><i class="fa fa-car"></i> Cadastro de Viaturas</h3>
                                Viaturas disponiveis na OM
                                <a class="btn btn-primary btn-flat btn-oliva pull-right"
                                   href="{{ route('viaturas.create') }}"><i class="fa fa-car"></i> Cadastrar Viaturas</a>
                            </div>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead class="table_blue">
                                    <tr>
                                        <th>Id</th>
                                        <th>Modelo</th>
                                        <th>Prefixo</th>
                                        <th>Registro EB / Placa</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($viaturas as $viatura)
                                        <tr>
                                            <td>{{ $viatura->id }}</td>
                                            <td>{{ $viatura->Modelo }}</td>
                                            <td>{{ $viatura->prefixo }}</td>
                                            <td>{{ $viatura->eb_placa }}</td>
                                            <td style="text-align: center;">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('viaturas.edit', $viatura->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Editar {{ $viatura->prefixo }}"><i
                                                                class="fa fa-pencil"></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('viaturas.show', $viatura->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Excluir Registro"> <i
                                                                class='fa fa-trash text-red'></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->            
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable(
                {
                    "language": {
                        "url": "/adminlte/components/datatables.net/js/ptBr.lang"
                    }
                }
            )
        });
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>    
@endpush