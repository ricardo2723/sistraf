@extends('layouts.mastertop')

@section('content')
<div class="row">
                <div class="col-md-3"></div>
                <div class="col-xs-12 col-md-6">
                        <div id="result"></div>
                        <div class="box box">
                                <div style="background-color: #808000; color:beige" class="box-header with-border">
                                            <h3><i class="fa fa-car"></i> Alterar Usuário</h3>
                                            {!! Form::model($user,['route' => ['usuarios.update', $user->id], 'method' => 'PUT']) !!}                                        
                                            <h5>Alterar Permissões de Usuários.</h5>                                            
                                    </div>  
                                    @include('layouts.alerts.validationAlert')                             
                                <div class="box-body">                                        
                                    <div class="rows">                                                                                        
                                        <div class="form-group">
                                            <label for="name">Nome: </label>
                                            {!! Form::text('name', $value = null, ['class' => 'form-control', 'autofocus']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Login: </label>
                                            {!! Form::text('username', $value = null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                <hr>
                                    <div class="rows">
                                        <h3>Lista de Acessos</h3>  
                                        <ul class="list-unstyled">                                 
                                            @foreach ($roles as $role)
                                                <li>    
                                                    <label>
                                                        {!! Form::checkbox('roles[]', $role->id, null) !!}
                                                        {{ $role->name }} - <em>({{ $role->description }})</em>
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>                                
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    {!! Form::submit('Alterar', ['class' => 'btn btn-oliva flat']) !!}
                                    <a href="{{ route('usuarios.index') }}" class="btn btn-oliva flat">Voltar</a>
                                </div>
                                {!! Form::close() !!}
                        </div><!-- /.box -->
                    </div><!-- /.col -->           
                </div>
@endsection
@push('datatables-css')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
@endpush

@push('datatables-script')
<!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
@endpush
