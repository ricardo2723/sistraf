@extends('layouts.mastertop')

@section('content')
    <!-- Modal -->
<div class="modal fade custom-modal" data-backdrop="static" id="modal-sucess" tabindex="1" role="dialog" aria-labelledby="customModal" aria-hidden="true">
    <div class="modal-dialog" style="width: 600px">
        <div class="modal-content">
            <div class="modal-header-delete">
                <h3>Excluir Usuário </h3>
            </div>
            <div class="modal-body">  
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-body">
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Usuário:</label>                                          
                                        <p>{{ $user->name }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Login</label>                                          
                                        <p>{{ $user->username }}</p>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>    
                </div>
            </div>
            <div class="modal-footer">                
                {!! Form::open(['route' => ['usuarios.destroy', $user->id], 'method' => 'DELETE']) !!}
                    {!! Form::submit('Excluir', ['class' => 'btn flat btn-danger']) !!}
                    <a href="{{ route('usuarios.index') }}" class="btn btn-primary flat">Voltar</a>
                {!! Form::close() !!}                
            </div>      
        </div>
    </div>
</div>		
@endsection

@push('datatables-css')
<!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
<!-- DataTables -->
<script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
<script>
    $(document).ready(function () {
            $('#modal-sucess').modal('show');
    });    
</script>
@endpush