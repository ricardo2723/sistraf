<?php

use Illuminate\Database\Seeder;
use App\Entities\Posto;

class PostosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Posto::create(
            [
                'name' => 'SOLDADO',
                'name_abreviado' => 'SD EP'
            ]
        );

        Posto::create(
            [
                'name' => 'SOLDADO EV',
                'name_abreviado' => 'SD EV'
            ]
        );

        Posto::create(
            [
                'name' => 'CABO',
                'name_abreviado' => 'CB'
            ]
        );
        Posto::create(
            [
                'name' => 'TAIFEIRO MOR',
                'name_abreviado' => 'TM'
            ]
        );
        Posto::create(
            [
                'name' => '3º SARGENTO',
                'name_abreviado' => '3º SGT'
            ]
        );
        Posto::create(
            [
                'name' => '2º SARGENTO',
                'name_abreviado' => '2º SGT'
            ]
        );
        Posto::create(
            [
                'name' => '1º SARGENTO',
                'name_abreviado' => '1º SGT'
            ]
        );
        Posto::create(
            [
                'name' => 'SUB TENENTE',
                'name_abreviado' => 'ST'
            ]
        );
        Posto::create(
            [
                'name' => 'ASPIRANTE',
                'name_abreviado' => 'ASP'
            ]
        );
        Posto::create(
            [
                'name' => '2º TENENTE',
                'name_abreviado' => '2º TEN'
            ]
        );
        Posto::create(
            [
                'name' => '1º TENENTE',
                'name_abreviado' => '1º TEN'
            ]
        );
        Posto::create(
            [
                'name' => 'CAPITÃO',
                'name_abreviado' => 'CAP'
            ]
        );
        Posto::create(
            [
                'name' => 'MAJOR',
                'name_abreviado' => 'MAJ'
            ]
        );
        Posto::create(
            [
                'name' => 'TENENTE CORONEL',
                'name_abreviado' => 'TC'
            ]
        );
        Posto::create(
            [
                'name' => 'CORONEL',
                'name_abreviado' => 'CEL'
            ]
        );
    }
}
