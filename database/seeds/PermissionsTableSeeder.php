<?php

use Illuminate\Database\Seeder;
use App\Entities\User;
use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {        
        //PERMISSÕES DO SISTEMA
        Permission::create([
            'name'          => 'Comandante da Guarda',
            'slug'          => 'cmtgda',
            'description'   => 'Terá acesso a todos os módulos para o Comandate da Guarda',
        ]);

        Permission::create([
            'name'          => 'Oficial de Dia',
            'slug'          => 'ofdia',
            'description'   => 'Terá acesso a todos os módulos para o Oficial de Dia',
        ]);

        Permission::create([
            'name'          => 'Gestor de Unidade',
            'slug'          => 'gestor',
            'description'   => 'Terá acesso aos Relatórios para o gerenciamento do Sistema',
        ]);

        //ROLES DO SISTEMA
        Role::create([
            'name'          => 'Administrador do Sistema',
            'slug'          => 'admin',
            'description'   => 'Terá acesso de Super Usuário a todos os módulos do sistema',
            'special'       => 'all-access',
        ]);
        
        Role::create([
            'name'          => 'Comandante da Guarda',
            'slug'          => 'cmtGda',
            'description'   => 'Terá acesso a todos os módulos para o Comandate da Guarda',
            'special'       => '0',
        ]);

        Role::create([
            'name'          => 'Oficial de Dia',
            'slug'          => 'ofDia',
            'description'   => 'Terá acesso a todos os módulos para o Oficial de Dia',
            'special'       => '0',
        ]);

        Role::create([
            'name'          => 'Gestor de Unidade',
            'slug'          => 'gestor',
            'description'   => 'Terá acesso aos Relatórios para o gerenciamento do Sistema',
            'special'       => '0',
        ]);      
        
        $user = User::find(1);
        $user->syncRoles([1]);
    }
}
