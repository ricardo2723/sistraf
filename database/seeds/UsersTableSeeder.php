<?php

use Illuminate\Database\Seeder;

use App\Entities\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create(
            [
                'name'      => 'Administrador do Sistema',
                'username'  => 'root',
                'password'  => bcrypt('01700244582520467170'),
            ]
        );
        /*
         *
         User::create(
            [
                'name'      => 'Francisco Ricardo',
                'username'  => 'SgtFrancisco',
                'password'  => bcrypt('123456'),
            ]
        );
        User::create(
            [
                'name'      => 'Fabiano Gomes',
                'username'  => 'TenGomes',
                'password'  => bcrypt('123456'),
            ]
        );        
        User::create(
            [
                'name'      => 'Gestor de Unidade',
                'username'  => 'Gestor',
                'password'  => bcrypt('123456'),
            ]
        );
        */
    }
}
