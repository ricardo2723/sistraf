<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateViaturasTable.
 */
class CreateViaturasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('viaturas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('modelo',45)->nullable();
            $table->string('prefixo', 45)->nullable();
            $table->string('eb_placa', 30)->nullable()->unique();
            $table->timestamps();
        });
	}

    public function down()
	{
		Schema::drop('viaturas');
	}
}
