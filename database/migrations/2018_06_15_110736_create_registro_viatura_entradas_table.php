<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRegistroViaturaEntradasTable.
 */
class CreateRegistroViaturaEntradasTable extends Migration
{

	public function up()
	{
		Schema::create('registro_viatura_entradas', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('registro_viatura_id');
            $table->foreign('registro_viatura_id')->references('id')->on('registro_viaturas')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('odom_entrada', 10);
            $table->datetime('odom_entrada_data');            
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('registro_viatura_entradas');
	}
}
