<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePostoGraduacaosTable.
 */
class CreatePostosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('postos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20)->unique();
            $table->string('name_abreviado', 10)->unique();

            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('postos');
	}
}
