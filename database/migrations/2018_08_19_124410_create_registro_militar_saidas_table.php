<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRegistroMilitarSaidasTable.
 */
class CreateRegistroMilitarSaidasTable extends Migration
{
	public function up()
	{
		Schema::create('registro_militar_saidas', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('registro_militar_id');
            $table->foreign('registro_militar_id')->references('id')->on('registro_militars')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->datetime('saida_data');
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('registro_militar_saidas');
	}
}
