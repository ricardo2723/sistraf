<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRegistroMilitarsTable.
 */
class CreateRegistroMilitarsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registro_militars', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services');
            $table->unsignedInteger('posto_id');
            $table->foreign('posto_id')->references('id')->on('postos');
            $table->string('nome_guerra', 45);
            $table->dateTime('entrada_data');
            $table->integer('veiculo');
            $table->integer('estacionamento');
            $table->string('selo_lacre_placa', 15)->nullable();
            $table->text('observacao')->nullable();
            $table->text('msg_correcao')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registro_militars');
	}
}
