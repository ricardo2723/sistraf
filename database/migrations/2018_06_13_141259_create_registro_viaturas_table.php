<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRegistroViaturasTable.
 */
class CreateRegistroViaturasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registro_viaturas', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('viatura_id');
			$table->foreign('viatura_id')->references('id')->on('viaturas');
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services');
            $table->string('cia', 6);
            $table->unsignedInteger('motorista_id');
            $table->foreign('motorista_id')->references('id')->on('motoristas');
            $table->unsignedInteger('chefe_viatura_id');
            $table->foreign('chefe_viatura_id')->references('id')->on('chefe_viaturas');
            $table->string('odom_saida', 10);
            $table->dateTime('odom_saida_data');
            $table->string('destino', 100);
            $table->text('observacao')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registro_viaturas');
	}
}
