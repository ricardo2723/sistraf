<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateChefeViaturasTable.
 */
class CreateChefeViaturasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chefe_viaturas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nome_completo', 75);
            $table->unsignedInteger('posto_id');
            $table->foreign('posto_id')->references('id')->on('postos');
            $table->string('nome_guerra', 30);
            $table->string('cpf',12)->nullable()->unique();
            $table->integer('status');

            $table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chefe_viaturas');
	}
}
