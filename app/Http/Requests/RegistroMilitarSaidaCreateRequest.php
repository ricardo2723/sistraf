<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroMilitarSaidaCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'saida_data' => 'required|date|after:data_inicio',
            'saida_hora' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'saida_data.after' => 'A Data da Saida deve ser maior que a data de Entrada',
            'saida_data.required' => 'A Data da Saida é obrigatória',
            'saida_data.data' => 'A Data informada não esta no formato correto',
            'saida_hora.required' => 'A Hora da Entrada é obrigatória',
        ];
    }

    public function validationData()
    {
        $dados = $this->all();

        $dados['saida_data'] = $dados['saida_data'] .' '. $dados['saida_hora'] . ':00';
        return $dados;
    }
}
