<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'          => 'required|min:6|max:100',
            'slug'          => 'required|min:3|max:20|unique:roles,slug',
            'description'   => 'min:6|max:100'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'O Nome é Obrigatório',
            'name.min'          => 'O Nome deve ter no Minimo 6 caracteres',
            'name.max'          => 'O Nome deve ter no Maximo 100 caracteres',
            'slug.required'     => 'O Perfil de Acesso é Obrigatório',
            'slug.min'          => 'O Perfil de Acesso deve ter no Minimo 3 caracteres',
            'slug.max'          => 'O Perfil de Acesso deve ter no Maximo 20 caracteres',
            'slug.unique'       => 'O Perfil de Acesso ja existe no Banco de Dados',
            'description.min'   => 'A Descrição deve ter no Minimo 6 caracteres',
            'description.max'   => 'A Descrição deve ter no Maximo 100 caracteres',
        ];
    }
}
