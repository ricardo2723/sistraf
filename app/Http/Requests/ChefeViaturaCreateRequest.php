<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChefeViaturaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_completo' => 'required|min:6|max:75',
            'nome_guerra'   => 'required|min:3|max:30',
            'cpf'           => 'nullable|numeric|digits_between:11,11|unique:chefe_viaturas,cpf',
            'status'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome_completo.required'    => 'O Nome é obrigatório',
            'nome_completo.min'         => 'O Nome deve ter no minimo 6 caracteres',
            'nome_completo.max'         => 'O Nome deve ter no maximo 75 caracteres',
            'nome_guerra.required'      => 'O Nome de Guerra é obrigatório',
            'nome_guerra.min'           => 'O Nome de Guerra deve ter no minimo 3 caracteres',
            'nome_guerra.max'           => 'O Nome de Guerra deve ter no maximo 30 caracteres',
            'cpf.numeric'               => 'O CPF deve conter apenas numeros sem espaço',
            'cpf.digits_between'        => 'O CPF deve conter 11 digitos',
            'cpf.unique'                => 'O CPF já existe no Banco de Dados',
            'status.required'           => 'O Status é obrigatório',
        ];
    }
}
