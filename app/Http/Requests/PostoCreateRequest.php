<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostoCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'              => 'required|max:20|min:04|unique:postos,name',
            'name_abreviado'    => 'required|max:10|min:02|unique:postos,name_abreviado',
        ];
    }

    public function messages()
    {
        return
        [
            'name.required'             =>   'O Posto / Graduação é obrigatório',
            'name.max'                  =>   'O Posto / Graduação deve conter no maximo 20 caracteres',
            'name.min'                  =>   'O Posto / Graduação deve conter no minimo 04 caracteres',
            'name.unique'               =>   'O Posto / Graduação já existe no Banco de Dados',
            'name_abreviado.required'   =>   'A Abreviação é obrigatório',
            'name_abreviado.max'        =>   'A Abreviação deve conter no maximo 10 caracteres',
            'name_abreviado.min'        =>   'A Abreviação deve conter no minimo 02 caracteres',
            'name_abreviado.unique'     =>   'A Abreviação já existe no Banco de Dados',
        ];
    }
}
