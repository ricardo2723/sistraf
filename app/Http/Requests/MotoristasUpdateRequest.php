<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Entities\Posto;

class MotoristasUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_completo' => 'required|min:6|max:145',
            'nome_guerra'   => 'required|min:6|max:45|unique:motoristas,nome_guerra,' . $this->motorista,
            'cpf'           => 'nullable|numeric|digits_between:11,11|unique:motoristas,cpf,' . $this->motorista,
            'status'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome_completo.required'    => 'O Nome é obrigatório',
            'nome_completo.min'         => 'O Nome deve ter no minimo 6 caracteres',
            'nome_completo.max'         => 'O Nome deve ter no maximo 145 caracteres',
            'nome_guerra.required'      => 'O Nome de Guerra é obrigatório',
            'nome_guerra.min'           => 'O Nome de Guerra deve ter no minimo 6 caracteres',
            'nome_guerra.max'           => 'O Nome de Guerra deve ter no maximo 45 caracteres',
            'nome_guerra.unique'        => 'O Nome de Guerra já existe no Banco de Dados',
            'cpf.numeric'               => 'O Cpf deve conter apenas numeros sem espaço',
            'cpf.digits_between'        => 'O Cpf deve conter 11 digitos',
            'cpf.unique'                => 'O CPF já existe no Banco de Dados',
            'status.required'         => 'O Status é obrigatório',
        ];
    }

    public function validationData()
    {
        $dados = $this->all();
        $posto = Posto::find($dados['posto_id']);
        $dados['nome_guerra'] = $posto->name_abreviado . ' ' .$dados['nome_guerra'];
        return $dados;
    }
}
