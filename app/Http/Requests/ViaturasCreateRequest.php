<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ViaturasCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'modelo'    => 'required|min:3',
            'prefixo'   => 'required',
            'eb_placa'  =>  'required|min:6|unique:viaturas,eb_placa',
        ];
    }

    public function messages()
    {
        return [
            'modelo.required'   => 'O Campo Modelo da Viatura é Obrigatório',
            'modelo.min'        => 'O Modelo da Viatura deve ter no minimo 3 caracteres',
            'prefixo.required'  => 'O Campo Prefixo é Obrigatório',
            'eb_placa.required' => 'O Campo Registro / Placa é Obrigatório',
            'eb_placa.min'      => 'O Registro / Placa deve ser maior que 6 caracteres',
            'eb_placa.unique'   => 'O Registro / Placa já está cadastrado no Banco de Dados',
        ];
    }
}
