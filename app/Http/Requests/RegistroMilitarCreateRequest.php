<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroMilitarCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'nome_guerra' => 'required|max:45',
            'entrada_data' => 'required|date|after_or_equal:data_inicio',
            'entrada_hora' => 'required',
            'veiculo' => 'required',
            'estacionamento' => 'required',
            'selo_lacre_placa' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome_guerra.required' => 'O Nome de Guerra é obrigatório',
            'nome_guerra.max' => 'O Nome de Guerra deve ter no Maximo 45 caracteres',
            'entrada_data.after_or_equal' => 'A Data da Entrada deve ser maior ou igual a data da abertura do Serviço',
            'entrada_data.required' => 'A Data da Entrada é Obrigatória',
            'entrada_data.data' => 'A Data informada não esta no formato correto',
            'entrada_hora.required' => 'A Hora da Entrada é Obrigatória',
            'veiculo.required' => 'O Tipo de Veiculo é obrigatorio',
            'estacionamento.required' => 'O Tipo de Estacionamento é obrigatório',
            'selo_lacre_placa.required' => 'O Selo / Lacre / Placa é obrigatório',
        ];
    }

    public function validationData()
    {
        $dados = $this->all();

        $dados['entrada_data'] = $dados['entrada_data'] .' '. $dados['entrada_hora'] . ':00';

        return $dados;
    }
}
