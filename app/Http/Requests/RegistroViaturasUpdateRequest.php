<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroViaturasUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'viatura_id' => 'required',
            'cia' => 'required',
            'motorista_id' => 'required',
            'chefe_viatura_id' => 'required',
            'destino' => 'required|min:4|max:100',
            'odom_saida' => 'digits_between:0,6|required|numeric',
            'odom_saida_data' => 'required|after_or_equal:data_inicio',
            'odom_saida_hora' => 'required',
        ];
    }

    public function validationData()
    {
        $dados = $this->all();

        $dados['odom_saida_data'] = $dados['odom_saida_data'] . ' ' . $dados['odom_saida_hora'];

        return $dados;
    }

    public function messages()
    {
        return [
            'viatura_id.required' => 'O Nome da Viatura é obrigatório',
            'motorista_id.required' => 'O Nome do Motoristas é obrigatório',
            'chefe_viatura_id.required' => 'O nome do Chefe de Viatura é obrigatório',
            'odom_saida.required' => 'O Odometro de Saida é obrigatório',
            'odom_saida.numeric' => 'O Odometro de Saida deve ter somente numeros',
            'odom_saida.digits_between' => 'O Odometro de Saida deve ter no maximo 6 Digitos',
            'odom_saida_data.required' => 'A Data da Saida é obrigatória',
            'odom_saida_hora.required' => 'A Hora da Saida é obrigatória',
            'destino.required' => 'O Destino é obrigatório',
            'destino.min' => 'O Destino deve ter no Minimo 4 caracteres',
            'destino.max' => 'O Destino deve ter no Maximo 100 caracteres',
        ];
    }
}
