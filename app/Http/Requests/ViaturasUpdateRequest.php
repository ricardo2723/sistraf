<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ViaturasUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'prefixo' => 'required',
            'eb_placa'  =>  'required|min:6|unique:viaturas,eb_placa,' . $this->viatura,
        ];
    }

    public function messages()
    {
        return [
            'prefixo.required'  => 'O Campo Prefixo é Obrigatório',
            'eb_placa.required' => 'O Campo Registro / Placa é Obrigatório',
            'eb_placa.min'      => 'O Registro / Placa deve ser maior que 6 caracteres',
            'eb_placa.unique'   => 'O Registro / Placa já está cadastrado no Banco de Dados',
        ];
    }
}
