<?php

namespace App\Http\Controllers;

use App\Repositories\PostoRepository;
use App\Services\MotoristasService;
use App\Http\Requests\MotoristasCreateRequest;
use App\Http\Requests\MotoristasUpdateRequest;
use App\Repositories\MotoristasRepository;

/**
 * Class MotoristasController.
 *
 * @package namespace App\Http\Controllers;
 */
class MotoristasController extends Controller
{
    protected $repository;
    protected $service;
    protected $postoRepository;

    public function __construct(MotoristasRepository $repository, MotoristasService $service, PostoRepository $postoRepository)
    {
        $this->middleware('permission:ofdia');
        $this->repository = $repository;
        $this->service = $service;
        $this->postoRepository = $postoRepository;
    }

    public function index()
    {
        $motoristas = $this->repository->all();
        return view('painel.motoristas.index', compact('motoristas'));
    }

    public function create()
    {
        $postoGraduacao = $this->postoRepository->all();
        return view('painel.motoristas.createEdit', compact('postoGraduacao'));
    }

    public function store(MotoristasCreateRequest $request)
    {
        if ($this->service->checaMotoristaCreate($request->all()))
            return redirect()->route('motoristas.index');

        $motorista = $this->service->store($request->all());

        session()->flash('success', [
            'success'   => $motorista['success'],
            'messages'  => $motorista['messages']
        ]);

        return redirect()->route('motoristas.index');
    }

    public function edit($id)
    {
        $motorista = $this->repository->find($id);
        $postoGraduacao = $this->postoRepository->all();

        return view('painel.motoristas.createEdit', compact('motorista', 'postoGraduacao'));
    }

    public function update(MotoristasUpdateRequest $request, $id)
    {
        if ($this->service->checaMotoristaUpdate($request->all(), $id))
            return redirect()->route('motoristas.index');

        $motorista = $this->service->update($request->all(), $id);

        session()->flash('success', [
           'success'    => $motorista['success'],
           'messages'  => $motorista['messages']
        ]);

        return redirect()->route('motoristas.index');
    }

    public function show($id)
    {
        $motorista = $this->repository->find($id);

        return view('painel.motoristas.show', compact('motorista'));
    }

    public function destroy($id)
    {
        $motorista = $this->service->destroy($id);

        session()->flash('success', [
            'success'   => $motorista['success'],
            'messages'  => $motorista['messages'],
        ]);

        return redirect()->route('motoristas.index');
    }
}
