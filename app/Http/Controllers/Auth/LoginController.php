<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Entities\Service;
use App\Entities\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validationLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = User::where('username', '=', $request->get('username'))->first();

        if(count($user->roles))
        {
            if(!isset($user))
            {
                $this->messagesAlert('É necessário um registro para acessar o sistema. Infome seus dados abaixo e tente novamente');
                return redirect()->route('register');
            }elseif(isset($user->roles[0]->slug) && $user->roles[0]->slug == 'admin' || $user->roles[0]->slug == 'gestor')
            {
                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }
            }
        }

        $serv = Service::where('status', '=', '0')->first();
        
        if(isset($serv))
        {                       
            if($serv->user->id != $user->id && $serv->userCmt->id != $user->id)
            { 
                $this->messagesAlert('Serviço encontra-se aberto pelo usuário ' . $serv->user->username);
                return redirect()->route('login');
            }
            elseif($serv->user->id == $user->id || $serv->userCmt->id == $user->id)
            {
                if ($this->attemptLogin($request)) {            
                    return $this->sendLoginResponse($request);
                }
            }
        }elseif(isset($user->roles[0]->slug)) 
        {
            if($user->roles[0]->slug == 'ofDia')
            {
                return redirect()->route('servico.openService', $user->id);                
            }else            
            {
                $this->messagesAlert('O Oficial de Dia ainda não Abriu o Serviço');
                return redirect()->route('login');
            }
        }else
        {
            $this->messagesAlert('Você não tem Autorização para Acesso');
            return redirect()->route('login');
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    
    protected function validationLogin(Request $request)
    {        
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], 
        [
            'username.required' => 'O nome de Usuário é Obrigatório.',            
            'password.required' => 'A senha do Usuário é Obrigatória',
        ]);    
    }

    public function messagesAlert($msg)
    {
        session()->flash('success', [
            'success'   => false,
            'messages'  => $msg
        ]);        
    }    
    
    public function username()
    {
        return 'username';
    }
}
