<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Entities\User;
use App\Services\UsersService;
use Caffeinated\Shinobi\Models\Role;

class UsersController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(UserRepository $repository, UsersService $service)
    {
        $this->middleware('permission::admin');
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index()
    {
        $users = $this->repository->all();

        return view('painel.users.index', compact('users'));
    }

    Public function show($id)
    {
        $user = $this->repository->find($id);
        return view('painel.users.show', compact('user')); 
    }
    
    public function edit($id)
    {
        $user = $this->repository->find($id);
        $roles = Role::get();

        return view('painel.users.edit', compact('user', 'roles'));
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $user = $this->service->update($request, $id);

        session()->flash('success', [
            'success'   => $user['success'],
            'messages'  => $user['messages']
        ]);

        return redirect()->route('usuarios.index');
    }

    public function destroy($id)
    {
        $request = $this->service->destroy($id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages'],
        ]);
        
        return redirect()->route('usuarios.index');
    }
}
