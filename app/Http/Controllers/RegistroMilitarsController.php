<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistroMilitarOficialUpdateRequest;
use App\Repositories\PostoRepository;
use App\Services\RegistroMilitarSaidasService;
use App\Services\RegistroMilitarsService;
use App\Http\Requests\RegistroMilitarCreateRequest;
use App\Http\Requests\RegistroMilitarUpdateRequest;
use App\Repositories\RegistroMilitarRepository;
use App\Services\ServicesService;

class RegistroMilitarsController extends Controller
{
    protected $repository;
    protected $postoRepository;
    protected $service;
    protected $militarSaidaService;
    protected $servicesService;

    public function __construct(RegistroMilitarRepository $repository, PostoRepository $postoRepository,
                                RegistroMilitarsService $service, RegistroMilitarSaidasService $militarSaidaService,
                                ServicesService $servicesService)
    {
        $this->middleware('permission:cmtgda');
        $this->repository = $repository;
        $this->postoRepository = $postoRepository;
        $this->service = $service;
        $this->militarSaidaService = $militarSaidaService;
        $this->servicesService = $servicesService;
    }

    public function index()
    {
        $registroMilitares = $this->repository->findWhereNotIn('status', [4]);

        return view('painel.registroMilitares.entradas.index', compact('registroMilitares'));

    }

    public function create()
    {
        $serv = $this->servicesService->getServicoUser(\Auth::user()->id);
        $postos = $this->postoRepository->all();

        return view('painel.registroMilitares.entradas.createEdit', compact('serv', 'postos'));
    }

    public function store(RegistroMilitarCreateRequest $request)
    {
        $registroMilitar = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $registroMilitar['success'],
            'messages' => $registroMilitar['messages']
        ]);

        return redirect()->route('registroMilitares.index');
    }

    public function edit($id)
    {
        $serv = $this->servicesService->getServicoUser(\Auth::user()->id);
        $postos = $this->postoRepository->all();
        $registroMilitarEdit = $this->repository->find($id);

        return view('painel.registroMilitares.entradas.createEdit', compact('postos', 'registroMilitarEdit', 'serv'));
    }

    public function update(RegistroMilitarUpdateRequest $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return redirect()->route('registroMilitares.index');
    }

    public function show($id)
    {
        $registroMilitar = $this->repository->find($id);

        return view('painel.registroMilitares.entradas.show', compact('registroMilitar'));
    }

    public function destroy($id)
    {
        $delete = $this->service->destroy($id);

        session()->flash('success', [
            'success' => $delete['success'],
            'messages' => $delete['messages']
        ]);

        return redirect()->route('registroMilitares.index');
    }

    public function corrigirRegistroShow($id)
    {
        $corrigirRegistroMilitar = $this->repository->find($id);
        $postos = $this->postoRepository->all();
        $serv = $this->servicesService->getServicoUser(\Auth::user()->id);

        return view('painel.registroMilitares.oficial.corrigirRegistro',
            compact('corrigirRegistroMilitar', 'postos', 'serv'));
    }

    public function corrigirRegistroUpdate(RegistroMilitarOficialUpdateRequest $request, $id)
    {
        $corrigirRegistro = $this->service->corrigirRegistroUpdate($request->all(), $id);

        if ($corrigirRegistro['success']) {
            $corrigirRegistro = $this->militarSaidaService->corrigirRegistroUpdate($request->all());

            session()->flash('success', [
                'success' => $corrigirRegistro['success'],
                'messages' => $corrigirRegistro['messages']
            ]);
        } else {
            session()->flash('success', [
                'success' => false,
                'messages' => 'Não foi possivel Corrigir os dados de Saida da Viatura.'
            ]);
        }
        return redirect()->route('registroMilitares.index');
    }
}
