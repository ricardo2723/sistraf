<?php

namespace App\Http\Controllers;

use App\Http\Requests\ViaturasCreateRequest;
use App\Http\Requests\ViaturasUpdateRequest;
use App\Repositories\ViaturasRepository;
use App\Services\ViaturasService;

class ViaturasController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(ViaturasRepository $repository, ViaturasService $service)
    {
        $this->middleware('permission:ofdia');
        $this->repository   = $repository;
        $this->service      = $service;
    }

    public function index()
    {
        $viaturas = $this->repository->all();

        return view('painel.viaturas.index', [
            'viaturas' => $viaturas
        ]);
    }

    public function create()
    {
        return view('painel.viaturas.createEdit');
    }

    public function store(ViaturasCreateRequest $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return redirect()->route('viaturas.index');
    }

    public function show($id)
    {
        $viatura = $this->repository->find($id);

        return view('painel.viaturas.show', compact('viatura'));
    }

    public function edit($id)
    {
        $viatura = $this->repository->find($id);

        return view('painel.viaturas.createEdit', compact('viatura'));
    }
    public function update(ViaturasUpdateRequest $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('viaturas.index');
    }

    public function destroy($id)
    {
        $request = $this->service->destroy($id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('viaturas.index');
    }
}
