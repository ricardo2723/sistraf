<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostoCreateRequest;
use App\Http\Requests\PostoUpdateRequest;
use App\Repositories\PostoRepository;
use App\Services\PostosService;

class PostosController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(PostoRepository $repository, PostosService $service)
    {
        $this->middleware('permission:admin');
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index()
    {
        $postoGraduacaos = $this->repository->all();

        return view('painel.posto.index', compact('postoGraduacaos'));
    }

    public function create()
    {
        return view('painel.posto.createEdit');
    }

    public function store(PostoCreateRequest $request)
    {
        $postoGraduacao = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $postoGraduacao['success'],
            'messages' => $postoGraduacao['messages'],
        ]);

        return redirect()->route('postograduacao.index');
    }

    public function edit($id)
    {
        $postoGraduacao = $this->repository->find($id);

        return view('painel.posto.createEdit', compact('postoGraduacao'));
    }

    public function update(PostoUpdateRequest $request, $id)
    {
        $postoGraduacao = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success' => $postoGraduacao['success'],
            'messages' => $postoGraduacao['messages'],
        ]);

        return redirect()->route('postograduacao.index');
    }

    public function show($id)
    {
        $postoGraduacao = $this->repository->find($id);

        return view('painel.posto.show', compact('postoGraduacao'));
    }

    public function destroy($id)
    {
        $deleted = $this->service->destroy($id);

        session()->flash('success',
            [
               'success' => $deleted['success'],
                'messages' => $deleted['messages'],
            ]);

        return redirect()->route('postograduacao.index');
    }
}
