<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ServiceCreateRequest;
use App\Http\Requests\ServiceUpdateRequest;
use App\Repositories\ServiceRepository;

use Caffeinated\Shinobi\Models\Role;
use App\Services\ServicesService;
use App\Repositories\RegistroViaturasRepository;


class ServicesController extends Controller
{
    protected $service;
    protected $registroViaturaRepository;

    public function __construct(ServicesService $service, RegistroViaturasRepository $registroViaturaRepository)
    {
        $this->middleware('auth')->only('closeService', 'showCloseService');
        $this->service = $service;
        $this->registroViaturaRepository = $registroViaturaRepository;
    }

    public function openService($user_id)
    {   
        if($this->service->checkIfServiceOpen() > 0)
        {
            session()->flash('success', [
                'success'   => false,
                'messages'  => 'Esse Tipo de Operação não é permitido pelo Sistema.'
            ]);

            return redirect()->route('login');
        } 

        $user = Role::where('slug', '!=', 'admin')->first();
        $users = $user->users->pluck('username', 'id');
        return view('painel.services.abrirServico', compact('users', 'user_id'));
    }

    public function store(ServiceCreateRequest $request)
    {
        $serv = $this->service->store($request);

        if($serv)
        {
            session()->flash('success',[
                'success'   => $serv,
                'messages'  => 'Serviço Aberto com Sucesso! Entre para acessar o Sistema.'
            ]);

            return redirect()->route('login');
        }
        
        session()->flash('success', [
            'success'   => false,
            'messages'  => $serv['messages']
        ]);

        return redirect()->route('servico.openService', compact($request['user_id']));
    }

    public function showCloseService()
    {
        return view('painel.services.closeService');
    }

    public function confirmaCloseService()
    {
        return view('painel.services.confirmaCloseService');
    }

    public function closeService()
    {
        
        if($this->registroViaturaRepository->findWhereIn('status', [1,2,3])->count() > 0)
        {
            session()->flash('success', [
                'success'   => false,
                'messages'  => 'Não Foi Possivel Fechar o Serviço, Existem Registros para Corrigir'
            ]);

            return view('painel.services.closeService');            
        }
        
        $serv = $this->service->closeService();
        if($serv)
        {
            \Auth::logout();

            session()->flash('success', [
                'success'   => $serv,
                'messages'  => 'Serviço Finalizado com Sucesso'
            ]);
            
            return redirect('/');
        }        
    }
}
