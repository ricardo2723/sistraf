<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostoRepository;
use App\Services\ChefeViaturasService;

use App\Http\Requests\ChefeViaturaCreateRequest;
use App\Http\Requests\ChefeViaturaUpdateRequest;
use App\Repositories\ChefeViaturaRepository;

class ChefeViaturasController extends Controller
{
    protected $repository;
    protected $service;
    protected $postoRepository;

    public function __construct(ChefeViaturaRepository $repository, ChefeViaturasService $service, PostoRepository $postoRepository)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->postoRepository = $postoRepository;
    }

    public function index()
    {
        $chefeViaturas = $this->repository->all();
        return view('painel.chefeViatura.index', compact('chefeViaturas'));
    }

    public function create()
    {
        $postoGraduacao = $this->postoRepository->findWhereNotIn('id', [1]);
        return view('painel.chefeViatura.createEdit', compact('postoGraduacao'));
    }

    public function store(ChefeViaturaCreateRequest $request)
    {
        if ($this->service->checaChefeViaturaCreate($request->all()))
            return redirect()->route('chefeviatura.index');

        $chefeviatura = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $chefeviatura['success'],
            'messages' => $chefeviatura['messages']
        ]);

        return redirect()->route('chefeviatura.index');
    }

    public function edit($id)
    {
        $chefeViatura = $this->repository->find($id);
        $postoGraduacao = $this->postoRepository->all();

        return view('painel.chefeViatura.createEdit', compact('chefeViatura', 'postoGraduacao'));
    }

    public function update(ChefeViaturaUpdateRequest $request, $id)
    {
        if ($this->service->checaChefeViaturaUpdate($request->all(), $id))
            return redirect()->route('chefeviatura.index');

        $chefeviatura = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success' => $chefeviatura['success'],
            'messages' => $chefeviatura['messages']
        ]);

        return redirect()->route('chefeviatura.index');
    }

    public function show($id)
    {
        $chefeViatura = $this->repository->find($id);

        return view('painel.chefeViatura.show', compact('chefeViatura'));
    }

    public function destroy($id)
    {
        $chefeviatura = $this->service->destroy($id);

        session()->flash('success', [
            'success' => $chefeviatura['success'],
            'messages' => $chefeviatura['messages'],
        ]);

        return redirect()->route('chefeviatura.index');
    }

}
