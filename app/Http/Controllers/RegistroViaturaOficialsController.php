<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RegistroViaturasUpdateRequest;
use App\Http\Requests\RegistroViaturaOficialUpdateRequest;
use App\Repositories\RegistroViaturasRepository;
use App\Services\RegistroViaturasService;
use App\Repositories\ViaturasRepository;
use App\Services\RegistroViaturaEntradasService;
use Caffeinated\Shinobi\Models\Permission;
use App\Services\ServicesService;
use Caffeinated\Shinobi\Models\Role;
use App\Services\ReportViaturasService;

class RegistroViaturaOficialsController extends Controller
{
    protected $repository;
    protected $serviceViatura;
    protected $viaturaRepository;
    protected $serviceServico;    
    protected $serviceReportViaturas;    

    
    public function __construct(RegistroViaturasRepository $repository, RegistroViaturasService $serviceViatura,
                                ViaturasRepository $viaturaRepository, ServicesService $serviceServico,
                                ReportViaturasService $serviceReportViaturas)
    {
        $this->middleware('permission:ofdia');

        $this->repository = $repository;
        $this->serviceViatura = $serviceViatura;
        $this->viaturaRepository = $viaturaRepository;        
        $this->serviceServico = $serviceServico;
        $this->serviceReportViaturas = $serviceReportViaturas;
    }

    public function index()
    {
        $registroViatura_list = $this->repository->findWhereNotIn('status', [4]);
        return view('painel.registroViaturas.oficial.index', compact('registroViatura_list'));
    }
    
    public function edit(Request $dados, $id)
    {
        $record = $this->serviceViatura->editRecord($dados->all(), $id);

        session()->flash('success', [
            'success'   => $record['success'],
            'messages'  => $record['messages']
        ]);

        return redirect()->route('registroViaturaOficial.index');
    }
    
    public function show($id)
    {
        return view('painel.registroViaturas.oficial.editObservacao', compact('id'));
    }
    
    public function finalizarRegistro($id)
    {
        $finish = $this->serviceViatura->finalizarRegistro($id);

        session()->flash('success', [
            'success'   => $finish['success'],
            'messages'  => $finish['messages']
        ]);

        return redirect()->route('registroViaturaOficial.index');
    }    
    
    public function viaturasPdf()
    {
        return  $this->serviceReportViaturas->viaturasPdf('registroViaturaOficial.gerarViaturasPdf');
    }

    public function gerarViaturasPdf()
    {
        return $this->serviceReportViaturas->gerarViaturasPdf();
    }

    public function trocarCmtGda()
    {
        $user = Role::where('slug', '!=', 'admin')->first();
        $users = $user->users->pluck('username', 'id');
        $serv = $this->serviceServico->getServicoNow();
        return view('painel.registroViaturas.oficial.trocarCmtGda', compact('users', 'serv'));
    }

    public function trocarCmtGdaUpdate(Request $request, $id)
    {
        $service = $this->serviceServico->trocarCmtGdaUpdate($request->all(), $id);

        session()->flash('success', [
            'success'   => $service['success'],
            'messages'  => $service['messages']
        ]);

        return redirect()->route('registroViaturaOficial.dashboard');
    }
}
