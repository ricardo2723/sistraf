<?php

namespace App\Http\Controllers;

use App\Repositories\RegistroMilitarRepository;
use App\Services\RegistroMilitarSaidasService;
use App\Http\Requests\RegistroMilitarSaidaCreateRequest;
use App\Http\Requests\RegistroMilitarSaidaUpdateRequest;

class RegistroMilitarSaidasController extends Controller
{
    protected $registroMilitarRepository;
    protected $service;

    public function __construct(RegistroMilitarRepository $registroMilitarRepository, RegistroMilitarSaidasService $service)
    {
        $this->registroMilitarRepository = $registroMilitarRepository;
        $this->service = $service;
    }

    public function show($id)
    {
        $registroMilitarSaida = $this->registroMilitarRepository->find($id);

        return view('painel.registroMilitares.saidas.closeRegistroMilitar', compact('registroMilitarSaida'));
    }

    public function store(RegistroMilitarSaidaCreateRequest $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return redirect()->route('registroMilitares.index');
    }

    public function update(RegistroMilitarSaidaUpdateRequest $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return redirect()->route('records.index');
    }
}
