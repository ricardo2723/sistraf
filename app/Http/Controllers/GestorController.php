<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\ViaturasRepository;
use Illuminate\Http\Request;
use App\Repositories\RegistroViaturasRepository;
use App\Services\ServicesService;
use App\Services\RegistroViaturasService;
use App\Services\ReportViaturasService;

class GestorController extends Controller
{
    protected $repositoryRegistroViatura;
    protected $serviceServico;
    protected $serviceViatura;
    protected $serviceReportViatura;
    protected $userRepository;
    protected $viaturaRepository;

    public function __construct(RegistroViaturasRepository $repositoryRegistroViatura, ServicesService $serviceServico,
                                RegistroViaturasService $serviceViatura, ReportViaturasService $serviceReportViatura,
                                UserRepository $userRepository, ViaturasRepository $viaturaRepository)
    {
        $this->middleware('permission:gestor');

        $this->repositoryRegistroViatura = $repositoryRegistroViatura;
        $this->serviceServico = $serviceServico;
        $this->serviceViatura = $serviceViatura;
        $this->serviceReportViatura = $serviceReportViatura;
        $this->userRepository = $userRepository;
        $this->viaturaRepository = $viaturaRepository;
    }

    public function dashboard()
    {
        $viaturasGraf = $this->serviceViatura->grafViaturasDashboard();
        $dia = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'];

        return view('painel.dashboard.gestor.dashboard',
            $this->serviceServico->dadosServicoDashboard(),
            $this->serviceViatura->dadosViaturaDashboard()
        )->with(compact('viaturasGraf', 'dia'));
    }

    //############################ RELATORIO SERVIÇO ############################

    public function servico()
    {
        return view('painel.registroViaturas.gestor.servico.service');
    }

    public function servicoShow(Request $request)
    {
        $request->validate(['data_servico' => 'required|date'],
            [
                'data_servico.required' => 'A Data do Serviço é obrigatória',
                'data_servico.date' => 'A Data do Serviço não é válida',
            ]);

        $servico = $this->serviceServico->getServicoData($request->all());

        if (is_null($servico))
            $error = true;

        return view('painel.registroViaturas.gestor.servico.service', compact('servico', 'error'));
    }

    public function servicoDataShow(Request $request)
    {
        $request->validate(['data_inicio' => 'required|date', 'data_final' => 'required|date'],
            [
                'data_inicio.required' => 'A Data Inicial é obrigatória',
                'data_inicio.date' => 'A Data Inicial não é válida',
                'data_final.required' => 'A Data Final é obrigatória',
                'data_final.date' => 'A Data Final não é válida',
            ]);

        $servicoBetween = $this->serviceServico->getServicoDataBetween($request->all());
        if ($servicoBetween->isEmpty())
            $error = true;

        return view('painel.registroViaturas.gestor.servico.service', compact('servicoBetween', 'error'));
    }

    public function servicoOficial()
    {
        $user_oficial = $this->userRepository->selectListBox();
        return view('painel.registroViaturas.gestor.servico.serviceOficial', compact('user_oficial'));
    }

    public function servicoOficialShow(Request $request)
    {
        $error = false;
        $servico = $this->serviceServico->getServicoOfDia($request->all());

        if ($servico->isEmpty())
            $error = true;

        return redirect()->back()->with('servico', [$servico])->with('error', [$error]);
    }

    public function servicoCmtGda()
    {
        $user_cmtGda = $this->userRepository->selectListBox();
        return view('painel.registroViaturas.gestor.servico.serviceCmtGda', compact('user_cmtGda'));
    }

    public function servicoCmtGdaShow(Request $request)
    {
        $error = false;
        $servico = $this->serviceServico->getServicoCmtGda($request->all());

        if ($servico->isEmpty())
            $error = true;

        return redirect()->back()->with('servico', [$servico])->with('error', [$error]);
    }

    //############################ RELATORIO VIATURA ############################

    public function viatura()
    {
        $viatura = $this->viaturaRepository->selectListBox();
        return view('painel.registroViaturas.gestor.viatura.viatura', compact('viatura'));
    }

    public function viaturaShow(Request $request)
    {
        $request->validate(['data_inicio' => 'required|date'],
            [
                'data_inicio.required' => 'A Data Inicial é obrigatória',
                'data_inicio.date' => 'A Data Inicial não é válida',
            ]);

        $viatura = $this->viaturaRepository->selectListBox();
        $viaturas = $this->serviceViatura->getRegistroViaturaData($request->all());
        $motoristasChefeViaturas = \App\Entities\Motorista::all();

        if(sizeof($viaturas) <= 0)
            $error = true;

        return view('painel.registroViaturas.gestor.viatura.viatura',
            compact('viatura', 'viaturas', 'motoristasChefeViaturas', 'error'));
    }

    public function viaturaDataShow(Request $request)
    {
        $request->validate(['data_inicio' => 'required|date', 'data_final' => 'required|date'],
            [
                'data_inicio.required' => 'A Data Inicial é obrigatória',
                'data_inicio.date' => 'A Data Inicial não é válida',
                'data_final.required' => 'A Data Final é obrigatória',
                'data_final.date' => 'A Data Final não é válida',
            ]);

        $viatura = $this->viaturaRepository->selectListBox();
        $viaturas = $this->serviceViatura->getRegistroViaturaDataBeteween($request->all());
        $motoristasChefeViaturas = \App\Entities\Motorista::all();

        if(sizeof($viaturas) <= 0)
            $error = true;

        return view('painel.registroViaturas.gestor.viatura.viatura',
            compact('viatura', 'viaturas', 'motoristasChefeViaturas', 'error'));
    }

    //############################ RELATORIO PDF ############################

    public function dashboardViaturasPdf()
    {
        return $this->serviceReportViatura->viaturasPdf('gestor.dashboardGerarViaturasPdf');
    }

    public function dashboardGerarViaturasPdf()
    {
        return $this->serviceReportViatura->gerarViaturasPdf();
    }

    public function viaturasPdf($id)
    {
        return $this->serviceReportViatura->viaturasPdfId('gestor.gerarViaturasPdf', $id);
    }

    public function gerarViaturasPdf($id)
    {
        return $this->serviceReportViatura->gerarViaturasPdfId($id);
    }

}
