<?php

namespace App\Http\Controllers;

use App\Repositories\RegistroMilitarRepository;
use App\Services\RegistroMilitarsService;
use Illuminate\Http\Request;

use App\Services\ServicesService;
use App\Services\ReportMilitarsService;

class RegistroMilitarOficialsController extends Controller
{
    protected $repository;
    protected $militarService;
    protected $servicoService;
    protected $reportMilitarsService;

    public function __construct(RegistroMilitarRepository $repository, RegistroMilitarsService $militarService,
                                ServicesService $servicoService, ReportMilitarsService $reportMilitarsService)
    {
        $this->middleware('permission:ofdia');

        $this->repository = $repository;
        $this->militarService = $militarService;
        $this->servicoService = $servicoService;
        $this->reportMilitarsService = $reportMilitarsService;
    }

    public function index()
    {
        $registroMilitar_list = $this->repository->findWhereNotIn('status', [4]);
        return view('painel.registroMilitares.oficial.index', compact('registroMilitar_list'));
    }

    public function show($id)
    {
        return view('painel.registroMilitares.oficial.editObservacao', compact('id'));
    }

    public function edit(Request $dados, $id)
    {
        $record = $this->militarService->editRecord($dados->all(), $id);

        session()->flash('success', [
            'success'   => $record['success'],
            'messages'  => $record['messages']
        ]);

        return redirect()->route('registroMilitarOficial.index');
    }

    public function finalizarRegistro($id)
    {
        $finish = $this->militarService->finalizarRegistro($id);

        session()->flash('success', [
            'success'   => $finish['success'],
            'messages'  => $finish['messages']
        ]);

        return redirect()->route('registroMilitarOficial.index');
    }

    public function militaresPdf()
    {
        return  $this->reportMilitarsService->militarsPdf('registroMilitarOficial.gerarViaturasPdf');
    }

    public function gerarMilitaresPdf()
    {
        return $this->reportMilitarsService->gerarMilitarsPdf();
    }

}
