<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Services\RegistroMilitarsService;
use App\Services\RegistroViaturasService;
use App\Services\ServicesService;

class DashboardCmtGdaController extends Controller
{
    protected $servicoService;
    protected $viaturasService;
    protected $militarsService;

    public function __construct(ServicesService $servicoService, RegistroViaturasService $viaturasService, RegistroMilitarsService $militarsService)
    {
        $this->middleware('permission:cmtgda');
        $this->servicoService = $servicoService;
        $this->viaturasService = $viaturasService;
        $this->militarsService = $militarsService;
    }

    public function index()
    {
        $servico = $this->servicoService->dadosServicoDashboard();
        $viaturas = $this->viaturasService->dadosViaturaDashboard();
        $militars = $this->militarsService->dadosMilitarDashboard();

        return view('painel.dashboard.cmtgda.dashboard', compact('servico', 'viaturas', 'militars'));

    }
}
