<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function login()
    {
        return view('auth.login');
    }

    public function logout()
    {
        return Auth::logout();
    }

    public function auth(Request $request)
    {
        if(Auth::attempt(['username' => $request['username'], 'password' => $request['password']]))
            return redirect()->intended('home');
        
            return redirect('login')
            ->withInput($request->only('username'))
            ->withErrors([
                'username' => 'Login ou Senha inválidos',
            ]);
    }

}
