<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChefeViaturaCreateRequest;
use App\Http\Requests\MotoristasCreateRequest;
use App\Repositories\ChefeViaturaRepository;
use App\Repositories\MotoristasRepository;
use App\Repositories\PostoRepository;
use App\Repositories\ViaturasRepository;
use App\Services\ChefeViaturasService;
use App\Services\MotoristasService;
use App\Services\RegistroViaturasService;
use App\Services\RegistroViaturaEntradasService;
use App\Http\Requests\RegistroViaturasCreateRequest;
use App\Http\Requests\RegistroViaturasUpdateRequest;
use App\Http\Requests\RegistroViaturaOficialUpdateRequest;
use App\Repositories\RegistroViaturasRepository;
use App\Entities\Service;
use App\Services\ServicesService;

class RegistroViaturasController extends Controller
{
    protected $repository;
    protected $viaturaRepository;
    protected $service;
    protected $serviceEntrada;
    protected $servicoService;
    protected $motoristaRepository;
    protected $serviceMotorista;
    protected $chefeViaturaRepository;
    protected $postoRepository;
    protected $serviceChefeViatura;

    public function __construct(RegistroViaturasRepository $repository, ViaturasRepository $viaturaRepository,
                                RegistroViaturaEntradasService $serviceEntrada, RegistroViaturasService $service,
                                ServicesService $servicoService, MotoristasRepository $motoristaRepository,
                                MotoristasService $serviceMotorista, ChefeViaturaRepository $chefeViaturaRepository,
                                PostoRepository $postoRepository, ChefeViaturasService $serviceChefeViatura)
    {
        $this->middleware('permission:cmtgda');
        $this->repository = $repository;
        $this->viaturaRepository = $viaturaRepository;
        $this->service = $service;
        $this->serviceEntrada = $serviceEntrada;
        $this->servicoService = $servicoService;
        $this->motoristaRepository = $motoristaRepository;
        $this->serviceMotorista = $serviceMotorista;
        $this->chefeViaturaRepository = $chefeViaturaRepository;
        $this->postoRepository = $postoRepository;
        $this->serviceChefeViatura = $serviceChefeViatura;
    }


    public function index()
    {
        $registroViaturas = $this->repository->findWhereNotIn('status', [4]);

        return view('painel.registroViaturas.saida.index', compact('registroViaturas'));
    }

    public function create()
    {
        $serv = Service::where('status', 0)->where('cmt_user_id', \Auth::user()->id)->first();
        $viaturas = $this->viaturaRepository->selectListBox();
        $motorista = $this->motoristaRepository->all();
        $chefeViatura = $this->chefeViaturaRepository->all();
        return view('painel.registroViaturas.saida.createEdit', compact('viaturas', 'serv', 'motorista', 'chefeViatura'));
    }

    public function store(RegistroViaturasCreateRequest $request)
    {
        $registroViatura = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $registroViatura['success'],
            'messages' => $registroViatura['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function edit($id)
    {
        $viaturas = $this->viaturaRepository->selectListBox();
        $motorista = $this->motoristaRepository->all();
        $chefeViatura = $this->chefeViaturaRepository->all();
        $registroViaturaEdit = $this->repository->find($id);

        return view('painel.registroViaturas.saida.createEdit', compact('viaturas', 'registroViaturaEdit', 'motorista', 'chefeViatura'));
    }

    public function update(RegistroViaturasUpdateRequest $request, $id)
    {
        $serv = Service::where('status', 0)->where('cmt_user_id', \Auth::user()->id)->first();
        $request['service_id'] = $serv->id;
        $request = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function show($id)
    {
        $registroViatura = $this->repository->find($id);

        return view('painel.registroViaturas.saida.show', compact('registroViatura'));
    }

    public function closeViatura($id)
    {
        $record_id = $id;
        return view('painel.registroViaturas.index', compact('record_id'));
    }

    public function destroy($id)
    {
        $delete = $this->service->destroy($id);

        session()->flash('success', [
            'success' => $delete['success'],
            'messages' => $delete['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function corrigirRegistroShow($id)
    {
        $corrigirRegistroViatura = $this->repository->find($id);
        $viatura_list = $this->viaturaRepository->selectListBox();
        $motorista = $this->motoristaRepository->all();
        $chefeViatura = $this->chefeViaturaRepository->all();

        return view('painel.registroViaturas.oficial.corrigirRegistro',
            compact('corrigirRegistroViatura', 'viatura_list', 'motorista', 'chefeViatura'));
    }

    public function corrigirRegistroUpdate(RegistroViaturaOficialUpdateRequest $request, $id)
    {
        $corrigirRegistro = $this->service->corrigirRegistroUpdate($request->all(), $id);

        if ($corrigirRegistro['success']) {
            $corrigirRegistro = $this->serviceEntrada->corrigirRegistroUpdate($request->all());

            session()->flash('success', [
                'success' => $corrigirRegistro['success'],
                'messages' => $corrigirRegistro['messages']
            ]);
        } else {
            session()->flash('success', [
                'success' => false,
                'messages' => 'Não foi possivel Corrigir os dados de Saida da Viatura.'
            ]);
        }
        return redirect()->route('registroViaturas.index');
    }

    public function motoristaCreate()
    {
        $postoGraduacao = $this->postoRepository->all();
        return view('painel.registroviaturas.saida.createMotorista', compact('postoGraduacao'));
    }

    public function motoristaStore(MotoristasCreateRequest $request)
    {
        $motorista = $this->serviceMotorista->store($request->all());

        session()->flash('success', [
            'success' => $motorista['success'],
            'messages' => $motorista['messages']
        ]);

        return redirect()->route('registroViaturas.create');
    }

    public function chefeViaturaCreate()
    {
        $postoGraduacao = $this->postoRepository->findWhereNotIn('id', [1]);
        return view('painel.registroviaturas.saida.createChefeViatura', compact('postoGraduacao'));
    }

    public function chefeViaturaStore(ChefeViaturaCreateRequest $request)
    {
        $motorista = $this->serviceChefeViatura->store($request->all());

        session()->flash('success', [
            'success' => $motorista['success'],
            'messages' => $motorista['messages']
        ]);

        return redirect()->route('registroViaturas.create');
    }

}
