<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('big', 'App\Rules\big@bigOdom');

        Validator::replacer('big', function($message, $attribute, $rule, $parameters){
            return 'O Odometro de Entrada deve ser maior que o de Saida';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
