<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RegistroViaturaEntrada extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['registro_viatura_id', 'user_id', 'odom_entrada', 'odom_entrada_data'];

    public function registroSaida()
    {
        return $this->belongsTo(RegistroViatura::class, 'id');
    }
    
    public function getFormattedOdomEntradaDataAttribute()
    {
        $data = $this->attributes['odom_entrada_data'];
        return (new \DateTime($data))->format('d/m/Y');
    }

    public function getFormattedOdomEntradaHoraAttribute()
    {
        $hora = $this->attributes['odom_entrada_data'];
        return (new \DateTime($hora))->format('H:i');
    }

    public function getFormattedUpOdomEntradaDataAttribute()
    {
        $odom_entrada_data = $this->attributes['odom_entrada_data'];
        return (new \DateTime($odom_entrada_data))->format('Y-m-d');
    }

    public function getFormattedUpOdomEntradaHoraAttribute()
    {
        $odom_entrada_data = $this->attributes['odom_entrada_data'];
        return (new \DateTime($odom_entrada_data))->format('H:i');
    }
}
