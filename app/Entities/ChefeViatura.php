<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ChefeViatura.
 *
 * @package namespace App\Entities;
 */
class ChefeViatura extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome_completo', 'posto_id', 'nome_guerra', 'cpf', 'status'];

    public function setNomeGuerraAttribute($value)
    {
        $this->attributes['nome_guerra'] = strtoupper($value);
    }

    public function getPostoName()
    {
        return $this->belongsTo(Posto::class, 'posto_id');
    }
}
