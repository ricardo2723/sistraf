<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RegistroMilitarSaida extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['registro_militar_id', 'user_id', 'saida_data' ];

    public function getFormattedSaidaDataAttribute()
    {
        return (new \DateTime($this->attributes['saida_data']))->format('d/m/y | H:i');
    }

    public function getFormattedUpSaidaDataAttribute()
    {
        return (new \DateTime($this->attributes['saida_data']))->format('Y-m-d');
    }

    public function getFormattedUpSaidaHoraAttribute()
    {
        return (new \DateTime($this->attributes['saida_data']))->format('H:i');
    }
}
