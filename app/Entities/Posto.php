<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;



class Posto extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name', 'name_abreviado'];

    public function getNameAttribute()
    {
        return strtoupper($this->attributes['name']);
    }

    public function getNameAbreviadoAttribute()
    {
        return strtoupper($this->attributes['name_abreviado']);
    }
}
