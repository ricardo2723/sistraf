<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RegistroViatura extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['viatura_id', 'user_id', 'service_id', 'cia', 'motorista_id', 'odom_saida', 'odom_saida_data', 'chefe_viatura_id', 'destino', 'observacao', 'status'];

    public function entradaViatura()
    {
        return $this->hasOne(RegistroViaturaEntrada::class);
    }

    public function motorista()
    {
       return $this->belongsTo(Motorista::class, 'motorista_id');
    }

    public function chefeViatura()
    {
        return $this->belongsTo(ChefeViatura::class, 'chefe_viatura_id');
    }

    public function detalhesViatura()
    {
        return $this->belongsTo(Viatura::class, 'viatura_id');
    }

    public function services()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function getFormattedOdomSaidaDataAttribute()
    {
        $odom_saida_data = $this->attributes['odom_saida_data'];
        return (new \DateTime($odom_saida_data))->format('d/m/Y');
    }

    public function getFormattedOdomSaidaHoraAttribute()
    {
        $odom_saida_hora = $this->attributes['odom_saida_data'];
        return (new \DateTime($odom_saida_hora))->format('H:i');
    }

    public function getFormattedUpOdomSaidaDataAttribute()
    {
        $odom_saida_data = $this->attributes['odom_saida_data'];
        return (new \DateTime($odom_saida_data))->format('Y-m-d');
    }

    public function getFormattedUpOdomSaidaHoraAttribute()
    {
        $odom_saida_data = $this->attributes['odom_saida_data'];
        return (new \DateTime($odom_saida_data))->format('H:i');
    }

}
