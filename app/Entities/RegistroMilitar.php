<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RegistroMilitar.
 *
 * @package namespace App\Entities;
 */
class RegistroMilitar extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['user_id', 'service_id', 'posto_id', 'nome_guerra', 'entrada_data', 'saida_data', 'veiculo', 'estacionamento', 'selo_lacre_placa', 'observacao', 'msg_correcao', 'status'];

    public function getPostoName()
    {
        return $this->belongsTo(Posto::class, 'posto_id');
    }

    public function getSaidaMilitar()
    {
        return $this->hasOne(RegistroMilitarSaida::class);
    }

    public function getFormattedEntradaDataAttribute()
    {
        $entrada_data = $this->attributes['entrada_data'];
        return (new \DateTime($entrada_data))->format('d/m/Y');
    }

    public function getFormattedEntradaHoraAttribute()
    {
        $entrada_data = $this->attributes['entrada_data'];
        return (new \DateTime($entrada_data))->format('H:i');
    }

    public function getNomeGuerraAttribute($value)
    {
        return strtoupper($value);
    }

    public function getFormattedUpEntradaDataAttribute()
    {
        $entrada_data = $this->attributes['entrada_data'];
        return (new \DateTime($entrada_data))->format('Y-m-d');
    }

    public function getFormattedUpEntradaHoraAttribute()
    {
        $entrada_data = $this->attributes['entrada_data'];
        return (new \DateTime($entrada_data))->format('H:i');
    }
}
