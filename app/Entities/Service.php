<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Service.
 *
 * @package namespace App\Entities;
 */
class Service extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'cmt_user_id', 'data_inicio', 'data_final', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userCmt()
    {
        return $this->belongsTo(User::class, 'cmt_user_id');
    }

    public function registroViatura()
    {
        return $this->hasMany(RegistroViatura::class);
    }

    public function getFormattedDataInicioAttribute()
    {
        $inicio = $this->attributes['data_inicio'];
        return (new \DateTime($inicio))->format('d/m/Y');
    }

    public function getFormattedDataFinalAttribute()
    {
        $final = $this->attributes['data_final'];
        return (new \DateTime($final))->format('d/m/Y');
    }

    public function getFormattedDataInicioDiaAttribute()
    {
        $dia = $this->attributes['data_inicio'];
        return (new \DateTime($dia))->format('d');
    }

    public function getFormattedDataFinalDiaAttribute()
    {
        $dia = $this->attributes['data_final'];
        return (new \DateTime($dia))->format('d');
    }

    public function getFormattedDataFinalMesAttribute()
    {
        return $this->getMesExtenso();
    }

    private function getMesExtenso()
    {
        $mes = (new \DateTime($this->attributes['data_final']))->format('m');

        switch ($mes) {
            case 01;
                return 'Janeiro';
            case 2:
                return 'fevereiro';
            case 3:
                return 'março';
            case 4:
                return 'abril';
            case 5:
                return 'maio';
            case 6:
                return 'junho';
            case 7:
                return 'julho';
            case 8:
                return 'agosto';
            case 9:
                return 'setembro';
            case 10:
                return 'outubro';
            case 11:
                return 'novembro';
            case 12:
                return 'dezembro';
        }
    }

}
