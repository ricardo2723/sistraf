<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Viatura extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['modelo', 'prefixo', 'eb_placa'];

    public function getPrefixoAttribute()
    {
        return strtoupper($this->attributes['prefixo']);
    }

    public function getEbPlacaAttribute()
    {
        return strtoupper($this->attributes['eb_placa']);
    }

    public function getModeloAttribute()
    {
        return strtoupper($this->attributes['modelo']);
    }

}
