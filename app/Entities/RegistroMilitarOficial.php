<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RegistroMilitarOficial.
 *
 * @package namespace App\Entities;
 */
class RegistroMilitarOficial extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function getPostoName()
    {
        return $this->belongsTo(Posto::class, 'posto_id');
    }

    public function getSaidaMilitar()
    {
        return $this->hasOne(RegistroMilitarSaida::class);
    }
}
