<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PostoGraduacaoRepository.
 *
 * @package namespace App\Repositories;
 */
interface PostoRepository extends RepositoryInterface
{
    //
}
