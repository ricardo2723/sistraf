<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegistroMilitarRepository.
 *
 * @package namespace App\Repositories;
 */
interface RegistroMilitarRepository extends RepositoryInterface
{
    //
}
