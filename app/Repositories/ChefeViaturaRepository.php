<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChefeViaturaRepository.
 *
 * @package namespace App\Repositories;
 */
interface ChefeViaturaRepository extends RepositoryInterface
{
    //
}
