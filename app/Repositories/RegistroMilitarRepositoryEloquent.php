<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RegistroMilitarRepository;
use App\Entities\RegistroMilitar;
use App\Validators\RegistroMilitarValidator;

/**
 * Class RegistroMilitarRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RegistroMilitarRepositoryEloquent extends BaseRepository implements RegistroMilitarRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RegistroMilitar::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
