<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegistroMilitarSaidaRepository.
 *
 * @package namespace App\Repositories;
 */
interface RegistroMilitarSaidaRepository extends RepositoryInterface
{
    //
}
