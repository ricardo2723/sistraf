<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegistroViaturasRepository.
 *
 * @package namespace App\Repositories;
 */
interface RegistroViaturasRepository extends RepositoryInterface
{
    //
}
