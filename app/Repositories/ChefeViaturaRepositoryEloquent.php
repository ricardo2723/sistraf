<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ChefeViaturaRepository;
use App\Entities\ChefeViatura;
use App\Validators\ChefeViaturaValidator;

/**
 * Class ChefeViaturaRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ChefeViaturaRepositoryEloquent extends BaseRepository implements ChefeViaturaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ChefeViatura::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
