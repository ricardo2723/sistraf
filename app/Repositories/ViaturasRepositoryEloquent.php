<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ViaturasRepository;
use App\Entities\Viatura;


class ViaturasRepositoryEloquent extends BaseRepository implements ViaturasRepository
{
    public function selectListBox(string $descricao = 'eb_placa', string $prefixo = 'prefixo', string $chave = 'id')
    {
        return $this->model->pluck($descricao, $chave)->all();
    }

    public function model()
    {
        return Viatura::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
