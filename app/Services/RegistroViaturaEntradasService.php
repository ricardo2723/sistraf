<?php

namespace App\Services;

use App\Entities\RegistroViatura;
use App\Exceptions\ExceptionsErros;

use App\Repositories\RegistroViaturaEntradasRepository;


class RegistroViaturaEntradasService
{
    protected $repository;
    protected $registroViaturaModel;
    protected $errors;
    protected $servico;

    public function __construct(RegistroViaturaEntradasRepository $repository, RegistroViatura $registroViaturaModel,
                                ExceptionsErros $errors, ServicesService $servico)
    {
        $this->repository = $repository;
        $this->registroViaturaModel = $registroViaturaModel;
        $this->errors = $errors;
        $this->servico = $servico;
    }

    public function store($dados)
    {
        try {
            $dados['odom_entrada_data'] = $dados['odom_entrada_data'] . ' ' . $dados['odom_entrada_hora'];
            $this->repository->create($dados);

            $registroViatura = $this->registroViaturaModel->find($dados['registro_viatura_id']);

            $id_serv = $this->servico->getServicoNow()->id;
            if ($registroViatura->service_id != $id_serv)
                $registroViatura->service_id = $id_serv;

            $registroViatura->status = 1;
            $registroViatura->save();

            return [
                'success' => true,
                'messages' => 'Entrada da Viatura Cadastrada com Sucesso',
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function update(array $dados, $id)
    {
        try {
            $record = $this->repository->update($dados, $id);

            return [
                'success' => true,
                'messages' => 'Entrada da Viatura Alterado com Sucesso',
                'data' => null,
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function corrigirRegistroUpdate(array $dados)
    {
        try {
            $dados['odom_entrada_data'] = $dados['odom_entrada_data'] . ' ' . $dados['odom_entrada_hora'];

            $this->repository->update($dados, $dados['id_entrada']);

            return [
                'success' => true,
                'messages' => 'Correção Cadastrada com Sucesso'
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function formatData($request)
    {
        $data = explode('/', $request->input('odom_entrada_data'));
        $data = $data[2] . '-' . $data[1] . '-' . $data[0];

        return $data;
    }

    public function formatOdomEntrada($request)
    {
        $odomSaida = str_replace('_', '', $request->input('odom_entrada'));

        return $odomSaida;
    }
}