<?php

namespace App\Services;

use App\Exceptions\ExceptionsErros;
use App\Repositories\RegistroViaturasRepository;
use App\Entities\RegistroViatura;
use App\Repositories\ServiceRepository;
use Illuminate\Support\Facades\DB;

class RegistroViaturasService
{
    private $repository;
    protected $errors;
    protected $registroViaturaModel;
    protected $repositoryService;
    protected $serv;

    public function __construct(RegistroViatura $registroViaturaModel, RegistroViaturasRepository $repository,
                                ExceptionsErros $errors, ServiceRepository $repositoryService)
    {
        $this->repository = $repository;
        $this->errors = $errors;
        $this->registroViaturaModel = $registroViaturaModel;
        $this->repositoryService = $repositoryService;
    }

    public function store($dados)
    {
        try {
            $dados['odom_saida_data'] = $dados['odom_saida_data'] . ' ' . $dados['odom_saida_hora'];

            $this->repository->create($dados);
            return [
                'success' => true,
                'messages' => 'Saida da Viatura Cadastrada com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function update(array $dados, $id)
    {
        try {
            $dados['odom_saida_data'] = $dados['odom_saida_data'] . ' ' . $dados['odom_saida_hora'];

            $this->repository->update($dados, $id);

            return [
                'success' => true,
                'messages' => 'Registro da Viatura Atualizado com Sucesso'
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function editRecord(array $dados, $id)
    {
        try {
            $registroViatura = $this->registroViaturaModel->find($id);
            $registroViatura->observacao = $dados['observacao'];
            $registroViatura->status = 2;
            $registroViatura->save();

            return [
                'success' => true,
                'messages' => 'Solicitação de Correção enviado com Sucesso'
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function corrigirRegistroUpdate(array $dados, $id)
    {
        try {
            $dados['status'] = 3;
            $dados['odom_saida_data'] = $dados['odom_saida_data'] . ' ' . $dados['odom_saida_hora'];

            $this->repository->update($dados, $id);

            return [
                'success' => true
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function storeEntrada($dados)
    {
        try {
            RecordViaturaEntrada::create($dados);

            return [
                'success' => true,
                'messages' => 'Entrada da Viaturada Cadastrada com Sucesso',
                'data' => null
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }

    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);

            return [
                'success' => true,
                'messages' => 'Registro de Viatura Excluido com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function finalizarRegistro($id)
    {
        try {
            $dados = $this->registroViaturaModel->find($id);
            $dados->status = 4;
            $dados->save();

            return [
                'success' => true,
                'messages' => 'Registro de Viatura Finalizado com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function dadosViaturaDashboard()
    {
        $this->serv = $this->repositoryService->findWhere(['status' => 0])->first();
        if (isset($this->serv)) {
            $viaturasServicoOld = $this->repository->findWhere([
                ['status', '<', 4],
                ['service_id', '!=', $this->serv->id],

            ]);

            $viaturasServico = $this->repository->findWhere([
                'service_id' => $this->serv->id,
            ]);

            $viaturasForaServico = $this->repository->findWhere([
                'status' => 0
            ]);

            $viaturasRetornoServico = $this->repository->findWhere([
                ['status', '<', 4],
                ['status', '>', 0]
            ]);

            return $data = [
                'viaturasServico' => $viaturasServico,
                'viaturasServicoOld' => $viaturasServicoOld,
                'viaturasForaServico' => $viaturasForaServico,
                'viaturasRetornoServico' => $viaturasRetornoServico,
            ];
        } else {
            return $data = [
                'viaturasServico' => null,
                'viaturasServicoOld' => null,
                'viaturasForaServico' => null,
                'viaturasRetornoServico' => null,
            ];
        }
    }

    //############################ GRÁFICOS  ############################

    public function grafViaturasDashboard()
    {
        //$semana = date("W", strtotime(date('Y-m-d')));
        //$ano = date("Y", strtotime(date('Y-m-d')));
        //return DB::select('SELECT count(id) as qtd, date_format(odom_saida_data, \'%Y-%m-%d\') as odom_saida_data from registro_viaturas  where week(odom_saida_data, 1) = ? and year(odom_saida_data)= ? group by date_format(odom_saida_data, \'%Y-%m-%d\')', [$semana, $ano]);

        return DB::select('SELECT count(id) as qtd, date_format(odom_saida_data, \'%Y-%m-%d\') as odom_saida_data from registro_viaturas  WHERE datediff(date(now()),odom_saida_data) < 7 group by date_format(odom_saida_data, \'%Y-%m-%d\')');
    }

    //############################ REPORT VIATURA  ############################

    public function getRegistroViaturaData(array $data)
    {
        return DB::select('SELECT * from registro_viaturas join viaturas on viatura_id = viaturas.id left join registro_viatura_entradas on registro_viatura_id = registro_viaturas.id where viatura_id = ? and date_format(odom_saida_data, \'%Y-%m-%d\') = ?', [$data['viatura'], $data['data_inicio']]);
    }

    public function getRegistroViaturaDataBeteween(array $data)
    {
        return DB::select('SELECT * from registro_viaturas join viaturas on viatura_id = viaturas.id left join registro_viatura_entradas on registro_viatura_id = registro_viaturas.id where viatura_id = ? and date_format(odom_saida_data, \'%Y-%m-%d\') >= ? and date_format(odom_saida_data, \'%Y-%m-%d\') <= ?', [$data['viatura'], $data['data_inicio'], $data['data_final']]);
    }

    //############################ PDF  ############################

    public function viaturasPdf($id)
    {
        $this->serv = $id;
        return $this->repository->scopeQuery(function ($query) {
            return $query->where('service_id', $this->serv)
                ->orWhere('status', 0);
        })->all();
    }

    public function viaturasPdfId($id)
    {
        $this->serv = $id;
        return $this->repository->scopeQuery(function ($query) {
            return $query->where('service_id', $this->serv);
        })->all();
    }


}
