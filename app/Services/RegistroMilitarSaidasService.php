<?php

namespace App\Services;

use App\Entities\RegistroMilitar;
use App\Exceptions\ExceptionsErros;

use App\Repositories\RegistroMilitarSaidaRepository;

class RegistroMilitarSaidasService
{
    protected $repository;
    protected $registroMilitarModel;
    protected $errors;
    protected $servico;

    public function __construct(RegistroMilitarSaidaRepository $repository, RegistroMilitar $registroMilitarModel,
                                ExceptionsErros $errors, ServicesService $servico)
    {
        $this->repository           = $repository;
        $this->registroMilitarModel = $registroMilitarModel;
        $this->errors               = $errors;
        $this->servico              = $servico;
    }

    public function store($dados)
    {
        try
        {
            $dados['saida_data'] = $dados['saida_data'] . ' ' . $dados['saida_hora'];

            $this->repository->create($dados);

            $registroMilitar = $this->registroMilitarModel->find($dados['registro_militar_id']);

            $id_serv = $this->servico->getServicoNow()->id;
            if($registroMilitar->service_id != $id_serv)
                $registroMilitar->service_id = $id_serv;

            $registroMilitar->status = 1;
            $registroMilitar->save();

            return [
                'success'   => true,
                'messages'  => 'Entrada da Viatura Cadastrada com Sucesso',
            ];

        }catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }

    public function corrigirRegistroUpdate(array $dados)
    {
        try
        {
            $dados['saida_data'] = $dados['saida_data'] . ' ' . $dados['saida_hora'];

            $this->repository->update($dados, $dados['id_saida']);

            return [
                'success'   => true,
                'messages'  => 'Correção Cadastrada com Sucesso'
            ];
        }catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }






    public function update(array $dados, $id)
    {
        try
        {
            $record = $this->repository->update($dados, $id);

            return [
                'success'   => true,
                'messages'  => 'Entrada da Viatura Alterado com Sucesso',
                'data'      => null,
            ];
        }catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }



    public function formatData($request)
    {
        $data = explode('/', $request->input('odom_entrada_data'));
        $data = $data[2] . '-' . $data[1] . '-' . $data[0];

        return $data;
    }

    public function formatOdomEntrada($request)
    {
        $odomSaida = str_replace('_', '', $request->input('odom_entrada'));

        return $odomSaida;
    }
}