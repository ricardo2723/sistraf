<?php

namespace App\Services;

use App\Exceptions\ExceptionsErros;
use App\Repositories\RegistroMilitarRepository;
use App\Repositories\ServiceRepository;
use Illuminate\Support\Facades\DB;

class RegistroMilitarsService
{
    private $repository;
    protected $errors;
    protected $repositoryService;
    protected $serv;

    public function __construct(RegistroMilitarRepository $repository, ExceptionsErros $errors,
                                ServiceRepository $repositoryService)
    {
        $this->repository = $repository;
        $this->errors = $errors;
        $this->repositoryService = $repositoryService;
    }

    public function store($dados)
    {
        try {
            $dados['entrada_data'] = $dados['entrada_data'] . ' ' . $dados['entrada_hora'];

            $this->repository->create($dados);
            return [
                'success' => true,
                'messages' => 'Entrada do Militar Cadastrado com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function update(array $dados, $id)
    {
        try {
            $dados['entrada_data'] = $dados['entrada_data'] . ' ' . $dados['entrada_hora'];

            $this->repository->update($dados, $id);

            return [
                'success' => true,
                'messages' => 'Registro do Militar Atualizado com Sucesso'
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);

            return [
                'success' => true,
                'messages' => 'Registro de Militar Excluido com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function editRecord(array $dados, $id)
    {
        try {
            $dados['status'] = 2;
            $this->repository->update($dados, $id);

            return [
                'success' => true,
                'messages' => 'Solicitação de Correção enviado com Sucesso'
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function finalizarRegistro($id)
    {
        try {
            $dados = ['status' => 4];

            $this->repository->update($dados, $id);

            return [
                'success' => true,
                'messages' => 'Registro de Militar Finalizado com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function corrigirRegistroUpdate(array $dados, $id)
    {
        try {
            $dados['status'] = 3;
            $dados['entrada_data'] = $dados['entrada_data'] . ' ' . $dados['entrada_hora'];

            $this->repository->update($dados, $id);

            return [
                'success' => true
            ];
        } catch (\Exception $e) {
            return $this->errors->errosExceptions($e);
        }
    }

    public function dadosMilitarDashboard()
    {
        $this->serv = $this->repositoryService->findWhere(['status' => 0])->first();
        if (isset($this->serv)) {

            $militarsServico = $this->repository->findWhere([
                'service_id' => $this->serv->id,
            ]);

            $militarsEntrada = $this->repository->findWhere([
                ['status', '<', 1]
            ]);

            $militarsSaida = $this->repository->findWhere([
                ['status', '>', 0],
                ['service_id', '=', $this->serv->id]
            ]);

            return $data = [
                'militarsServico' => $militarsServico,
                'militarsSaida' => $militarsSaida,
                'militarsEntrada' => $militarsEntrada,
            ];
        } else {
            return $data = [
                'militarsServico' => null,
                'militarsSaida' => null,
                'militarsEntrada' => null,
            ];
        }
    }

    //############################ PDF  ############################

    public function militarsPdf($id)
    {
        $this->serv = $id;
        return $this->repository->scopeQuery(function ($query) {
            return $query->where('service_id', $this->serv)
                ->orWhere('status', 0);
        })->all();
    }

    public function viaturasPdfId($id)
    {
        $this->serv = $id;
        return $this->repository->scopeQuery(function ($query) {
            return $query->where('service_id', $this->serv);
        })->all();
    }







    //############################ GRÁFICOS  ############################

    public function grafMilitarsDashboard()
    {
        //$semana = date("W", strtotime(date('Y-m-d')));
        //$ano = date("Y", strtotime(date('Y-m-d')));
        //return DB::select('SELECT count(id) as qtd, date_format(odom_saida_data, \'%Y-%m-%d\') as odom_saida_data from registro_viaturas  where week(odom_saida_data, 1) = ? and year(odom_saida_data)= ? group by date_format(odom_saida_data, \'%Y-%m-%d\')', [$semana, $ano]);

        return DB::select('SELECT count(id) as qtd, date_format(odom_saida_data, \'%Y-%m-%d\') as odom_saida_data from registro_viaturas  WHERE datediff(date(now()),odom_saida_data) < 7 group by date_format(odom_saida_data, \'%Y-%m-%d\')');
    }

    //############################ REPORT VIATURA  ############################

    public function getRegistroMilitarData(array $data)
    {
        return DB::select('SELECT * from registro_viaturas join viaturas on viatura_id = viaturas.id left join registro_viatura_entradas on registro_viatura_id = registro_viaturas.id where viatura_id = ? and date_format(odom_saida_data, \'%Y-%m-%d\') = ?', [$data['viatura'], $data['data_inicio']]);
    }

    public function getRegistroMilitarDataBeteween(array $data)
    {
        return DB::select('SELECT * from registro_viaturas join viaturas on viatura_id = viaturas.id left join registro_viatura_entradas on registro_viatura_id = registro_viaturas.id where viatura_id = ? and date_format(odom_saida_data, \'%Y-%m-%d\') >= ? and date_format(odom_saida_data, \'%Y-%m-%d\') <= ?', [$data['viatura'], $data['data_inicio'], $data['data_final']]);
    }



}
