<?php
/**
 * Created by PhpStorm.
 * User: SGT FRANCISCO
 * Date: 26/07/2018
 * Time: 10:23
 */

namespace App\Services;


use App\Exceptions\ExceptionsErros;
use App\Repositories\MotoristasRepository;

class MotoristasService
{
    protected $repository;
    protected $erros;

    public function __construct(MotoristasRepository $repository, ExceptionsErros $erros)
    {
        $this->repository = $repository;
        $this->erros = $erros;
    }

    public function store($dados)
    {
        try {

            $this->repository->create($dados);

            return [
                'success' => true,
                'messages' => 'Motorista Cadastrado com Sucesso'
            ];

        } catch (\Exception $e) {
            return $this->erros->errosExceptions($e);
        }

    }

    public function update($dados, $id)
    {
        try
        {
            $this->repository->update($dados, $id);

            return [
              'success'     => true,
              'messages'    => 'Motorista Alterado com Sucesso'
            ];

        }catch (\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try
        {
            $this->repository->delete($id);

            return [
                'success'   => true,
                'messages'  => 'Motorista | Chefe de Viatura Excluido com Sucesso!'
            ];

        }catch (\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
    }

    public function checaMotoristaCreate(array $request)
    {
        $check = $this->repository->findWhere([
            'posto_id' => $request['posto_id'],
            'nome_guerra' => $request['nome_guerra']
        ]);

        if (count($check) > 0) {
            session()->flash('success', [
                'success' => false,
                'messages' => 'Não foi Possivel Cadastrar o Motorista, o nome de guerra ja está cadastrado no Banco de Dados'
            ]);

            return true;
        }

        return false;
    }

    public function checaMotoristaUpdate(array $request, $id)
    {
        $check = $this->repository->findWhere([
            'posto_id' => $request['posto_id'],
            'nome_guerra' => $request['nome_guerra'],
        ])->first();

        if (count($check) > 0) {
            if($check->id != $id)
            {
                session()->flash('success', [
                    'success' => false,
                    'messages' => 'Não foi Possivel Cadastrar o Motorista, o nome de guerra ja está cadastrado no Banco de Dados'
                ]);

                return true;
            }
            return false;
        }
        return false;
    }
}