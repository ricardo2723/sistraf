<?php

namespace App\Services;

use App\Exceptions\ExceptionsErros;

class ReportMilitarsService
{
    protected $servicoService;
    protected $militarsService;
    protected $erros;

    public function __construct(ServicesService $servicoService, ExceptionsErros $erros,
                                RegistroMilitarsService $militarsService)
    {
        $this->servicoService = $servicoService;
        $this->militarsService = $militarsService;
        $this->erros = $erros;

    }

    public function militarsPdf($route)
    {
        $militars = $this->servicoService->getServicoNow();
        $servicoMilitars = $this->militarsService->militarsPdf($militars->id);
        return view('painel.registroMilitares.pdf.militaresPdf', compact('militars', 'servicoMilitars', 'route'));
    }

    public function gerarMilitarsPdf()
    {
        $militars = $this->servicoService->getServicoNow();
        $servicoMilitars = $this->militarsService->militarsPdf($militars->id);
        return \PDF::loadView('painel.registroMilitares.pdf.gerarMilitaresPdf', compact('militars', 'servicoMilitars'))
            ->setPaper('a4', 'landscape')
            ->stream($militars->data_inicio . '_' . 'FichasMilitars.pdf');
    }



    public function viaturasPdfId($route, $id)
    {
        $viaturas = $this->serviceServico->getServicoId($id);
        $servicoViaturas = $this->serviceViatura->viaturasPdfId($id);
        return view('painel.registroViaturas.pdf.viaturasPdf', compact('viaturas', 'servicoViaturas', 'route', 'id'));
    }

    public function gerarViaturasPdfId($id)
    {
        $viaturas = $this->serviceServico->getServicoId($id);
        $servicoViaturas = $this->serviceViatura->viaturasPdfId($id);
        return \PDF::loadView('painel.registroViaturas.pdf.gerarViaturasPdf', compact('viaturas', 'servicoViaturas'))
            ->setPaper('a4', 'landscape')
            ->stream($viaturas->data_inicio . '_' . 'FichasViaturas.pdf');
    }

}