<?php

namespace App\Services;

use App\Services\RegistroViaturasService;
use App\Services\ServicesService;

use App\Exceptions\ExceptionsErros;

class ReportViaturasService
{
    protected $serviceServico;
    protected $serviceViatura;
    protected $erros;
    
    public function __construct(ServicesService $serviceServico, ExceptionsErros $erros, 
                                RegistroViaturasService $serviceViatura)
    {
        $this->serviceServico   = $serviceServico;
        $this->serviceViatura   = $serviceViatura;
        $this->erros            = $erros;
    }

    public function viaturasPdf($route)
    {
        $viaturas = $this->serviceServico->getServicoNow();
        $servicoViaturas = $this->serviceViatura->viaturasPdf($viaturas->id);
        return view('painel.registroViaturas.pdf.viaturasPdf', compact('viaturas', 'servicoViaturas', 'route'));
    }
    
    public function gerarViaturasPdf()
    {
        $viaturas = $this->serviceServico->getServicoNow();
        $servicoViaturas = $this->serviceViatura->viaturasPdf($viaturas->id);
        return \PDF::loadView('painel.registroViaturas.pdf.gerarViaturasPdf', compact('viaturas', 'servicoViaturas'))
            ->setPaper('a4', 'landscape')
             ->stream($viaturas->data_inicio . '_' . 'FichasViaturas.pdf');
    }

    public function viaturasPdfId($route, $id)
    {
        $viaturas = $this->serviceServico->getServicoId($id);
        $servicoViaturas = $this->serviceViatura->viaturasPdfId($id);
        return view('painel.registroViaturas.pdf.viaturasPdf', compact('viaturas', 'servicoViaturas', 'route', 'id'));
    }

    public function gerarViaturasPdfId($id)
    {
        $viaturas = $this->serviceServico->getServicoId($id);
        $servicoViaturas = $this->serviceViatura->viaturasPdfId($id);
        return \PDF::loadView('painel.registroViaturas.pdf.gerarViaturasPdf', compact('viaturas', 'servicoViaturas'))
            ->setPaper('a4', 'landscape')
            ->stream($viaturas->data_inicio . '_' . 'FichasViaturas.pdf');
    }
    
}