<?php

namespace App\Services;

use App\Repositories\ServiceRepository;
use App\Exceptions\ExceptionsErros;

class ServicesService
{
    protected $repository;
    protected $erros;
    
    public function __construct(ServiceRepository $repository, ExceptionsErros $erros)
    {
        $this->repository                   = $repository;
        $this->erros                        = $erros;
    }

    public function store($data)
    {
        $data_final = date('Y-m-d', strtotime('+1 days', strtotime($data['data_inicio'])));
        $data['data_inicio'] = $data['data_inicio'] . ' 08:00:00';
        $data['data_final'] = $data_final . ' 07:59:00';
        
        try
        {
            $this->repository->create($data->all());
            return true;

        }catch(\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
        
    }

    public function closeService()
    {
        try
        {
            $serv = $this->repository->findWhere(['status' => '0', 'user_id' => \Auth::user()->id])->first();
            $serv->status = 1;
            
            $serv->update();
            
            return true;

        }catch(\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
        
    }

    public function dadosServicoDashboard()
    {
        $servico = [
            'servico' => $this->repository->findWhereIn('status', [0])->first(),
        ];

        return $servico;
    }    


    public function trocarCmtGdaUpdate(array $dados, $id)
    {
        try
        {
            $this->repository->update($dados, $id);

            return [
                'success'   => true,
                'messages'  => 'Comandante da Guarda Alterado com Sucesso'
            ];

        }catch(\Exceptions $e)
        {
            return $this->erros->errosExceptions($e);
        }
    }

    public function checkIfServiceOpen()
    {
        return $this->repository->findWhereIn('status', [0])->count();
    }

    public function getServicoNow()
    {
        return $this->repository->findWhere(['status' => '0'])->first();
    }

    public function getServicoUser($id_user)
    {
        return $this->repository->findWhere(['cmt_user_id' => $id_user, 'status' => '0'])->first();
    }

    public function getServicoId($id)
    {
        return $this->repository->findWhere(['id' => $id])->first();
    }
    
    public function getServicoData(array $data)
    {
        return $this->repository->findWhere(['data_inicio' => $data['data_servico'] . ' 08:00:00'])->first();
    }

    public function getServicoDataBetween(array $data)
    {
        return $this->repository->findWhere([['data_inicio', '>=', $data['data_inicio'] . ' 08:00:00'], ['data_inicio', '<=', $data['data_final'] . ' 08:00:00']]);
    }

    public function getServicoOfDia(array $data)
    {
        return $this->repository->findWhere(['user_id' => $data['of_dia']]);
    }

    public function getServicoCmtGda(array $data)
    {
        return $this->repository->findWhere(['cmt_user_id' => $data['cmt_gda']]);
    }

}