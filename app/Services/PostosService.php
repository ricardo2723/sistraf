<?php
/**
 * Created by PhpStorm.
 * User: SGT FRANCISCO
 * Date: 09/08/2018
 * Time: 09:39
 */

namespace App\Services;


use App\Repositories\PostoRepository;
use Psy\Exception\ErrorException;

class PostosService
{
    protected $repository;
    protected $erros;

    public function __construct(PostoRepository $repository, ErrorException $erros)
    {
        $this->repository = $repository;
        $this->erros = $erros;
    }

    public function store(array $request)
    {
        try {
            $this->repository->create($request);

            return
                [
                    'success' => true,
                    'messages' => 'Posto / Graduação cadastrado com sucesso',
                ];

        } catch (\Exception $e) {
            return $this->erros->errosExceptions($e);
        }
    }

    public function update(array $request, $id)
    {
        try {
            $this->repository->update($request, $id);

            return
                [
                    'success' => true,
                    'messages' => 'O Posto / Graduação foi Alterado com Sucesso',
                ];

        } catch (\Exception $e) {
            return $this->erros->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try {
            $this->repository->delete($id);

            return
                [
                    'success' => true,
                    'messages' => 'O Posto / Graduação foi Excluido com Sucesso',
                ];

        } catch (\Exception $e) {
            return $this->erros->errosExceptions($e);
        }
    }
}