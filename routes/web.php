<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// ############################# USERS #################################################
// Route::get('/login', 'DashboardController@login')->name('user.login');
// Route::post('/login', 'DashboardController@auth')->name('user.logar');
// Route::get('/logout', 'DashboardController@logout')->name('user.logout');

// ############################# SERVIÇOS ###############################################
Route::resource('servico', 'ServicesController');
Route::get('/servico/user/{user}', 'ServicesController@openService')->name('servico.openService');
Route::get('/showCloseService', 'ServicesController@showCloseService')->name('servico.showCloseService');
Route::get('/closeService', 'ServicesController@closeService')->name('servico.closeService');
Route::get('/confirmaCloseService', 'ServicesController@confirmaCloseService')->name('servico.confirmaCloseService');

// ############################### ROTAS AUTENTICADAS ####################################
Route::middleware('auth')->group(function () {

// ################################ HOME #################################################
    Route::get('/home', 'HomeController@index')->name('home');

// ##################################### DASHBOARD #################################################
    Route::any('/dashboard/CmtGda', 'Dashboard\DashboardCmtGdaController@index')->name('dashboard.cmtgda');
    Route::any('/dashboard/OfiDia', 'Dashboard\DashboardOfiDiaController@index')->name('dashboard.ofidia');

// ############################# USUARIOS #################################################
    Route::resource('usuarios', 'UsersController');

// ############################## POSTO / GRADUAÇÃO #######################################
    Route::resource('postograduacao', 'PostosController');

// ############################## MOTORISTA ###############################################
    Route::resource('motoristas', 'MotoristasController');

// ############################### CHEFE DE VIATURA #######################################
    Route::resource('chefeviatura', 'ChefeViaturasController');

// ############################## ACESSOS #################################################
    Route::resource('acessos', 'RolesController');

// ############################# VIATURAS #################################################
    Route::resource('viaturas', 'ViaturasController');

// ############################# REGISTRO DE SAIDA DE VIATURAS #############################
    Route::resource('registroViaturas', 'RegistroViaturasController');
    Route::get('/registroViatura/corrigir/{registroViaturaOficial}', 'RegistroViaturasController@corrigirRegistroShow')->name('registroViatura.corrigirRegistro');
    Route::put('/registroViatura/{registroViaturaOficial}/update', 'RegistroViaturasController@corrigirRegistroUpdate')->name('registroViatura.corrigirRegistroUpdate');
    Route::get('/registroViatura/motorista', 'RegistroViaturasController@motoristaCreate')->name('registroViatura.motoristaCreate');
    Route::post('/registroViatura/motorista', 'RegistroViaturasController@motoristaStore')->name('registroViatura.motoristaStore');
    Route::get('/registroViatura/chefeviatura', 'RegistroViaturasController@chefeViaturaCreate')->name('registroViatura.chefeViaturaCreate');
    Route::post('/registroViatura/chefeviatura', 'RegistroViaturasController@chefeViaturaStore')->name('registroViatura.chefeViaturaStore');

// ############################# REGISTRO DE ENTRADA DE VIATURA ############################
    Route::resource('registroViaturaEntradas', 'RegistroViaturaEntradasController');

// ############################# REGISTRO DE ENTRADA DE MILITARES #############################
    Route::resource('registroMilitares', 'RegistroMilitarsController');
    Route::get('/registroMilitares/corrigir/{registroViaturaOficial}', 'RegistroMilitarsController@corrigirRegistroShow')->name('registroMilitares.corrigirRegistro');
    Route::put('/registroMilitares/{registromilitaresOficial}/update', 'RegistroMilitarsController@corrigirRegistroUpdate')->name('registroMilitares.corrigirRegistroUpdate');

// ############################# REGISTRO DE SAIDA DE VIATURA ############################
    Route::resource('registroMilitarSaidas', 'RegistroMilitarSaidasController');

// ############################# REGISTRO DE VIATURA OFICIAL ###############################
    Route::get('/registroViaturaOficial', 'RegistroViaturaOficialsController@index')->name('registroViaturaOficial.index');
    Route::get('/registroViaturaOficial/{registroViaturaOficial}', 'RegistroViaturaOficialsController@show')->name('registroViaturaOficial.show');
    Route::put('/registroViaturaOficial/{registroViaturaOficial}/edit', 'RegistroViaturaOficialsController@edit')->name('registroViaturaOficial.edit');
    Route::get('/registroViaturaOficial/finalizarRegistro/{finalizarRegistro}', 'RegistroViaturaOficialsController@finalizarRegistro')->name('registroViaturaOficial.finalizarRegistro');
    Route::get('/trocarCmtGda', 'RegistroViaturaOficialsController@trocarCmtGda')->name('registroViaturaOficial.trocarCmtGda');
    Route::put('/trocarCmtGda/{trocarCmtGda}/update', 'RegistroViaturaOficialsController@trocarCmtGdaUpdate')->name('registroViaturaOficial.trocarCmtGdaUpdate');
    Route::any('/registroViaturaOficial/relatorios/pdf', 'RegistroViaturaOficialsController@viaturasPdf')->name('registroViaturaOficial.viaturasPdf');
    Route::any('/registroViaturaOficial/relatorios/pdf/gerar', 'RegistroViaturaOficialsController@gerarViaturasPdf')->name('registroViaturaOficial.gerarViaturasPdf');

// ############################# REGISTRO DE MILITARES OFICIAL ###############################
    Route::get('/registroMilitarOficial', 'RegistroMilitarOficialsController@index')->name('registroMilitarOficial.index');
    Route::get('/registroMilitarOficial/{registroMilitarOficial}', 'RegistroMilitarOficialsController@show')->name('registroMilitarOficial.show');
    Route::put('/registroMilitarOficial/{registroMilitarOficial}/edit', 'RegistroMilitarOficialsController@edit')->name('registroMilitarOficial.edit');
    Route::get('/registroMilitarOficial/finalizarRegistro/{finalizarRegistro}', 'RegistroMilitarOficialsController@finalizarRegistro')->name('registroMilitarOficial.finalizarRegistro');
    Route::any('/registroMilitarOficial/relatorios/pdf', 'RegistroMilitarOficialsController@militaresPdf')->name('registroMilitarOficial.militaresPdf');
    Route::any('/registroMilitarOficial/relatorios/pdf/gerar', 'RegistroMilitarOficialsController@gerarMilitaresPdf')->name('registroMilitarOficial.gerarViaturasPdf');

// ######################################### GESTOR #########################################
    Route::get('/dashboard/gestor', 'GestorController@dashboard')->name('gestor.dashboard');
    Route::get('/report/viaturas', 'GestorController@dashboardViaturasPdf')->name('gestor.dashboardViaturasPdf');
    Route::get('/report/viaturas/pdf', 'GestorController@dashboardGerarViaturasPdf')->name('gestor.dashboardGerarViaturasPdf');
    Route::get('/report/viaturas/{report}', 'GestorController@viaturasPdf')->name('gestor.viaturasPdf');
    Route::get('/report/viaturas/pdf/{report}', 'GestorController@gerarViaturasPdf')->name('gestor.gerarViaturasPdf');
    // ################### SERVIÇO ###################
    Route::get('/report/servico', 'GestorController@servico')->name('gestor.reportServico');
    Route::post('/report/servicoShow', 'GestorController@servicoShow')->name('gestor.reportServicoShow');
    Route::post('/report/servicoDataShow', 'GestorController@servicoDataShow')->name('gestor.reportServicoDataShow');
    // ################### SERVIÇO OFICIAL ###################
    Route::get('/report/servico/oficial', 'GestorController@servicoOficial')->name('gestor.reportServicoOficial');
    Route::post('/report/servico/oficialShow', 'GestorController@servicoOficialShow')->name('gestor.reportServicoOficialShow');
    // ################### SERVIÇO CMT GDA ###################
    Route::get('/report/servico/cmtGda', 'GestorController@servicoCmtGda')->name('gestor.reportServicoCmtGda');
    Route::post('/report/servico/cmtGdaShow', 'GestorController@servicoCmtGdaShow')->name('gestor.reportServicoCmtGdaShow');
    // ################### VIATURA ###################
    Route::get('/report/viatura', 'GestorController@viatura')->name('gestor.reportViatura');
    Route::post('/report/viaturaShow', 'GestorController@viaturaShow')->name('gestor.reportViaturaShow');
    Route::post('/report/viaturaDataShow', 'GestorController@viaturaDataShow')->name('gestor.reportViaturaDataShow');
});